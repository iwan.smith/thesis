for SOURCE in `ls *tex`
do

	NAME=`sed -e 's/.tex//g' <<< $SOURCE`
	echo $NAME
	if [ ! -f $NAME"-crop.pdf" ]
	then
		latex   $NAME.tex
		dvips   $NAME.dvi
		ps2pdf  $NAME.ps
		pdfcrop $NAME.pdf
	fi
	if [ $NAME".tex" -nt $NAME"-crop.pdf" ]
	then
		latex   $NAME.tex
		dvips   $NAME.dvi
		ps2pdf  $NAME.ps
		pdfcrop $NAME.pdf
	fi
	
	
done
