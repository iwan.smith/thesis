MAIN = thesis
PLAN = plan
LATEXDRAFT = pdflatex --draftmode -shell-escape -output-directory=$(1) $(2) 
LATEX      = pdflatex             -shell-escape -output-directory=$(1) $(2) 
BIBTEX = bibtex -terse $(1) 

MAINEXT= .pdf
BUILDCOMMAND = $(call LATEXDRAFT,$(2),$(1)) && $(call BIBTEX,$(1)) && $(call LATEXDRAFT,$(2),$(1)) && $(call LATEX,$(2),$(1))
#BUILDCOMMAND = @rubber -d $(1)

# list of all source files
TEXSOURCES = $(shell find . -type f -name "*.tex")
BIBSOURCES = $(shell find . -type f -name "*.bib")
BSTSOURCES = $(shell find . -type f -name "*.bst")
STYSOURCES = $(shell find . -type f -name "*.sty")
FIGSOURCES = $(shell find ./figs/ -type f -name "*.pdf")
OTHSOURCES = $(BIBSOURCES) $(BSTSOURCES) $(STYSOURCES) $(FIGSOURCES)

SEPSOURCES = $(shell find . -type f -name "standalone.tex")
SEPOUTPUTS = $(patsubst ./body/%/standalone.tex, ./chapters/%$(MAINEXT), $(SEPSOURCES))

BUILDFILES = *.aux *.log *.bbl *.blg *.dvi *.tmp *.out *.blg *.bbl *.toc *.lof *.lot

OUTPUT = $(SEPOUTPUTS) $(MAIN)$(MAINEXT)


all: Feynman $(OUTPUT) 
	@rm -f $(BUILDFILES)

debug: all

plan: $(PLAN)$(MAINEXT)
	@echo "Building $@"
	@rm -f $(BUILDFILES)

$(MAIN)$(MAINEXT): $(TEXSOURCES) $(OTHSOURCES)
	@echo "Building $@"
	@$(call BUILDCOMMAND,$(MAIN),.)

$(PLAN)$(MAINEXT): body/abstract.tex body/*/plan.tex $(STYSOURCES)
	@echo "Building $@"
	@$(call BUILDCOMMAND,$(PLAN),.)

chapters/%$(MAINEXT): body/%/standalone.tex $(OTHSOURCES) body/%/*.tex head/*.tex | chapters
	@echo "Building $@"
	@$(call BUILDCOMMAND,body/$*/standalone,./body/$*/)
	@mv body/$*/standalone$(MAINEXT) $@
	@cd body/$* && rm -f $(BUILDFILES)

chapters:
	@mkdir -p $@

Feynman:
	cd  JaxoDraw && bash BuildPDFs.sh
	

.PHONY : clean
clean: tidy rmout


.PHONY : tidy
tidy:
	@rm -f $(BUILDFILES)

.PHONY: rmout
rmout:
	@rm -f $(OUTPUT) $(PLAN)$(MAINEXT) Graphs/*
