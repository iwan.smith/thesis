pdftotext thesis.pdf thesis.txt

rm Errors_* 2>/dev/null
touch Errors_NoRef.txt Errors_ToDo.txt

# Check for any bad references
cat thesis.txt| grep ?? >> Errors_NoRef.txt
cat thesis.txt| grep -i "the the" |grep -i -v "the theor" >> Errors_NoRef.txt

cat thesis.txt| grep -i ToDo >> Errors_ToDo.txt
cat thesis.txt| grep -i "To Do" >> Errors_ToDo.txt

# search for mislabeled labels
for f in `find -iname '*.tex'`
do
	cat $f| grep "label.*subsec" && echo $f
done >> Errors_NoRef.txt


# search for double labeled labels

OLDLABEL="OLDLABEL"
for LABEL in $(cat `find -iname '*.tex'`|grep \\label\{ |tr -d '[:blank:]'|sort)
do
	if [ $LABEL == $OLDLABEL ]
	then
		echo $LABEL
	fi
	OLDLABEL=$LABEL
done >> Errors_NoRef.txt

rm thesis.txt
