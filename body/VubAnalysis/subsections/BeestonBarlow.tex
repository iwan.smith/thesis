\section{Fit Method}
\label{sec:FitMethodBB}

\subsection{Beeston Barlow Fit Method}
\label{ssec:BB_NoUncert}

The Beeston-Barlow method for fitting using finite Monte Carlo samples~\cite{BARLOW1993219,Cranmer:1456844} is a binned template fit method used to extract the yields of different components from a data sample. Instead of using an analytical form for the shapes of the contributions a discrete histogram is used, dividing the distribution into $n$ bins.

The total number of events in data, $N_D$, and total number of events in the $j^{th}$ Monte Carlo template are found by summing over the $n$ bins of the template histogram,
\begin{equation}
 	N_D=\sum_{i=1}^n d_i, \hspace*{8mm} N_{MC_j} = \sum_{i=1}^na_{ji},
 \end{equation} 
 where $d_i$ and $a_{ji}$ are the number of data and Monte Carlo events in bin $i$ respectively.
%giving a set of numbers ${d_1, d_2,...,d_n}$ where $d_i$ is the data yield in the $i^{th}$ bin. 
Given $m$ fit components with fractional proportions, $f_j$, The predicted number of events in the $i^{th}$ bin of the data template, $n_i(f_1, f_2, ..., f_m)$, is 
\begin{equation}
	n_i = N_D \sum_{j=1}^{m} f_j a_{ji}/N_{MC_j},
	\label{eq:SumBins}
\end{equation}
where $N_D$ is the total data yield, and $a_{ji}$ the number of Monte Carlo events from source $j$ in bin $i$. The fractional proportions must sum to unity, 
\begin{equation}
	\sum_{j=1}^m f_j = 1.
\end{equation}
The proportions of each component, $p_j=N_Df_j/N_{MC_j}$, can be used, allowing Equation~\ref{eq:SumBins} to be rewritten,
\begin{equation}
	n_i = \sum_{j=1}^{m} p_j a_{ji},
	\label{eq:SumFractions}
\end{equation}
where the sum of proportions need not equal unity. The proportions scale the Monte Carlo template to its size in data.


%The yield in data, $N_D$, and total number of Monte Carlo events for component $j$, $N_{MC_j}$, form
%\begin{equation}
%	N_D = \sum_{i=1}^{n}d_i , \hspace*{5mm} N_{MC_j}=\sum_{i=1}^{n}a_{ji}.
%\end{equation}
The fractional proportions of each component, $f_j$, sum to unity and can be estimated by minimising
\begin{equation}
	\chi^2 = \sum_{i}\frac{(d_i-n_i)^2}{d_i},
	\label{eq:Chi2Fit}
\end{equation}
which assumes $d_i$ follows a Gaussian distribution. Truly $d_i$ follows a Poisson distribution, and with large numbers of events this is not a bad approximation, however there are often many bins with a low number of events making this approximation invalid.
One approach would be to use a binned maximum likelihood fit where the probability of observing a particular $d_i$ multiplied over all $n$ bins is
\begin{equation}
	\mathcal{L} = \prod_{i=1}^n e^{-n_i} \frac{n_i^{d_i}}{d_i!},
\end{equation}
and the estimates of the fractions, $f_j$ can be found my maximising the likelihood, or for convenience, the logarithm of the likelihood,
\begin{equation}
	\ln(\mathcal{L}) = \sum_{i=1}^n d_i ln(n_i) - n_i.
\end{equation}


The methods detailed above only consider the statistical uncertainties in the data sample and neglects any variation in the bin contents of the Monte Carlo templates. As a rule of thumb the Monte Carlo samples should contain $\approx 10$ times the number of events in the data, however due to the computational cost of generating simulated Monte Carlo events, the space required to store the data, and impracticalities handling massive datasets this rule is rarely observed. An approach is therefore needed which considers the statistical fluctuations in the Monte Carlo datasets.

The uncertainty parameter of the \chisq formalism shown in Equation~\ref{eq:Chi2Fit} can be modified to include the Monte Carlo uncertainty,
\begin{equation}
	\chi^2 = \sum_{i}\frac{(d_i-n_i)^2}{d_i+N_D^2\sum_ja_{ji}/N_{MC_j}^2},
\end{equation}
however this still uses the Gaussian approximation which is invalid when bins contain a low number of events.

In order to fully consider the statistical uncertainty from both the data and Monte Carlo, Equation~\ref{eq:SumFractions} can be rewritten replacing $a_{ji}$ with Poisson distributions, $A_{ji}$, 
\begin{equation}
	n_i = \sum_{j=1}^mp_jA_{ji}.
\end{equation}

The total likelihood is now the combined probability of the observed $d_i$ and $a_{ji}$,
\begin{equation}
	\ln(\mathcal{L})=\sum_{i=1}^nd_iln(n_i)-n_i + \sum_{i=1}^n\sum_{j=1}^ma_{ji}ln(A_{ji})-A_{ji}.
\end{equation}

During the construction of the likelihood the Poisson distributions of all template histograms are combined into a single Poisson distribution, thus there is only one Poisson distribution for each bin. For this step to work correctly the initial values of the proportions must be close to the values determined by the fit.

