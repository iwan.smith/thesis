\section{\boldmath \BsToDsMuNu Fit Results}
\label{sec:NormFit}



\subsection{Normalisation Fit Model}
\label{ssec:NF_Model}

A maximum likelihood, Beeston-Barlow binned template fit is performed on the corrected \Dsm\mup mass distribution to extract the \BsToDsMuNu signal yield. A bias free background subtraction is performed using the \Km\Kp\pim invariant mass distribution to remove the \Km\Kp\pim combinatoric contribution, the plots of which are shown in Figure~\ref{fig:Sub_Ds_Fits}. The yield in each bin of the corrected \Dsm\mup input histogram is the result of a fit to the \Km\Kp\mun invariant mass to determine the \Ds yield. The fit to the corrected \Dsm\mup mass distribution is used to separate the signal \BsToDsMuNu signal component from the background contributions. The backgrounds predominantly originate from semileptonic \Bs decays containing higher excited \Dsm resonances. Backgrounds consisting of partially reconstructed ${\B\to\Ds\D X}$ candidates and tauonic decays are considered, as are candidates containing misidentified muons. Combinatoric combinations of real muons with real \Ds mesons are neglected; when investigating the \Km\Kp\pim invariant mass distribution using the same sign, \Dsm\mun, sample no \Dsm peak is seen. The same sign sample may be assumed to be purely combinatoric as very few decays contain a same sign \Dsm and muon. The fit components and the sources of templates used in the fit are summarised in Table~\ref{Tab:NormFitComponents}. 

The templates used in the fit contain 40 bins in corrected mass ranging from 3000\mev to 6500\mev with an equal bin width. Sections~\ref{ssec:ParticleID} onwards discuss the corrections applied to Monte Carlo simulation to correct the selection efficiencies. It is important to note that when producing histograms for the fit the entries in each bin are weighted by kinematic, PID and tracking corrections detailed in Sections~\ref{subsec:KinematicCorrections},~\ref{ssec:ParticleID} and ~\ref{ssec:TrackingCorr}.
Backgrounds originating from similar decays with low yields are combined into a common template; all \B\to\Ds\D backgrounds are combined into a single template and the higher excitations of the \Ds above the \Dss are combined into single template. The combinations are plotted in Figure~\ref{fig:DsMuNuCombinations}. When creating the Monte Carlo templates for the fit all events are weighted by the product of the weights obtained from the BDT kinematic reweighting, PID correction and tracking  correction. When performing the fit to obtain the \BsToDsMuNu yield all component yields are left free.

\begin{table}[]
\centering
	\begin{tabular}{l|l}
		Component & Source   \\
		\hline
		$\Bs\to\Dsm\mup\neum$                                    & Monte Carlo   \\
		$\Bs\to\Dssm\mup\neum$, with $\Dssm\to\Dsm\g$            & Monte Carlo   \\
		$\Bs\to\Dssm\mup\neum$, with Higher \Ds Excitations      & Monte Carlo   \\
		$\Bs\to\Dsm\taup\neum X$, with $\taup\to\mup\neum\neutb$ & Monte Carlo   \\
		$B_q^0\to\Dsm D_q^{(*)}X$, with $D_q\to\mup\neum X$      & Monte Carlo   \\
		Misidentified Muons                                      & Fake Muon Data   \\
	\end{tabular}
	\caption[\BsToDsMuNu data sources for fit templates.]{Fit components for the normalisation fit and sources of data used when drawing templates.}
	\label{Tab:NormFitComponents}
\end{table}


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.49\textwidth, page=1]{Fit/DsMuNuTemplates.pdf}
	\includegraphics[width=0.49\textwidth, page=5]{Fit/DsMuNuTemplates.pdf}
	\caption[\BsToDsMuNu fit templates merged into a single template.]{The $\B\to\D\D\mup\neum X$ templates are merged into a single template, and the decays of further excitations of the \Ds are combined into a single template.}
	\label{fig:DsMuNuCombinations}
\end{figure}



\begin{figure}[htbp]
	\centering
	\hspace*{-10mm}
	\vspace*{-10mm}
	\includegraphics[width=1.1\textwidth]{body/VubAnalysis/figs/Ds_Fits.pdf}
	\vspace*{2mm}
	\caption[\Dsm\to\Km\Kp\pim background subtraction fits.]{Fits performed as part of a combinatoric background subtraction on the $\Kp\Km\pip$ invariant mass with pulls underneath. }
	\label{fig:Sub_Ds_Fits}
\end{figure}


\subsection{Background Subtraction}
\label{ssec:NF_BGSub}

A signal extraction is performed to remove the $\Kp\Km\pip$ combinatoric contribution from the data. Each data point in the {\Dsm\mup} corected mass input histogram for candidates in data, plotted in Figure~\ref{fig:NormFit}, is the result of a fit to the {\Kp\Km\pim} invariant mass distribution plotted in Figure~\ref{fig:Sub_Ds_Fits} to determine the \Dsm yield.
Correlations between the $\Kp\Km\pip$ invariant mass and the \Dsm\mup corrected mass mean that the \sPlot method for subtracting backgrounds cannot be used. Instead a \textit{divide and fit} method is used whereby the data is divided into $n$ smaller subsets, each corresponding to a specific bin in the \Dsm\mup corrected mass spectrum. A binned maximum likelihood fit is performed to the $\Kp\Km\pim$ invariant mass distribution for each dataset. A double-Gaussian models the \Dsm shape and an exponential models the combinatoric background shape. The \Ds\mup yield in the corrected mass histogram for each bin is set as the signal yield from the fit. The fits from the \textit{divide and fit} method are plotted in Figure~\ref{fig:Sub_Ds_Fits}. No background subtraction is required for the Monte Carlo samples.


\begin{comment}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.49\textwidth, page=2]{body/VubAnalysis/figs/BsMCorr.pdf}
	\includegraphics[width=0.49\textwidth, page=1]{body/VubAnalysis/figs/BsMCorr.pdf}
	\caption{ $\Kp\Km\pim\mup$ Corrected mass distributions before (left) and after (right) subtracting the combinatoric $\Kp\Km\pim$ contribution. No PID selections are applied to the fake muon and wrong sign ($\Dsm\mun$) distributions, which will further suppress these distributions.}
	\label{fig:MCorrBeforeAfter}
\end{figure}
\end{comment}




\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\textwidth]{Fit/FitPlot.pdf}
	\caption[Plotted \BsToDsMuNu fit results.]{A fit to the \Dsm\mup corrected mass for candidates in data passing the selections. The grey shaded boxes display the uncertainty in the fit model's predicted yield due to the finite Monte Carlo statistics. %The pulls, ${(n_{\mathrm{data}}^{i} - n_{\mathrm{model}}^{i}) /\sqrt{(\sigma_{\mathrm{data}}^i)^2 + (\sigma_{\mathrm{model}}^i)^2})}$, are plotted beneath the fit. 
	}
	\label{fig:NormFit}
\end{figure}




\subsection{Fit Results}
\label{ssec:NF_Results}


\begin{table}
	\centering
	\setlength{\tabcolsep}{2pt}
	\begin{tabular}{rcl|rcl}
		\multicolumn{3}{l}{Sample}         & \multicolumn{3}{l}{Yield / $10^3$}\\\hline
		$\Bs$&\to&$\Ds\mup\neum$                     & $197.9$&$\pm$&$11.9$\\
        $\Bs$&\to&$\Dssm\mup\neum$            & $366.0$&$\pm$&$17.5$\\
        $\Bs$&\to&$ D_{s0,1,2}^{*-}\mup\neum$ & $ 21.1$&$\pm$&$14.5$\\
        $\Bs$&\to&$\Ds\taup\neum$             & $ 21.3$&$\pm$&$4.1$\\
        $\B $&\to&$\Ds\D X$                    & $ 37.0$&$\pm$&$13.7$\\
        $\Bs$&\to&$\Ds~\mathrm{Fake}(\mup)X$  & $  0.6$&$\pm$&$1.3$\\
	\end{tabular}
	\caption[\BsToDsMuNu fit results.]{Fit results for all components of the fit used to obtain the \BsToDsMuNu yield.}
	\label{Tab:DsFitResults}
\end{table}


The results of the maximum likelihood fit to the \Ds\mup corrected mass are plotted in Figure~\ref{fig:NormFit}, fitting to all events passing the signal selection. The pulls, defined for the $i^{th}$ bin as the difference between data and model predictions, $n_{\mathrm{data}}^{i}$, and $n_{\mathrm{model}}^{i}$  respectively and their uncertainties, $\sigma_{\mathrm{data}}^i$ and $\sigma_{\mathrm{model}}^i$ respectively, are defined as,
\begin{equation}
	\frac{n_{\mathrm{data}}^{i} - n_{\mathrm{model}}^{i}}{\sqrt{(\sigma_{\mathrm{data}}^i)^2 + (\sigma_{\mathrm{model}}^i)^2}}.
\end{equation}
The \BsToDsMuNu yield is found to be $(197.9\pm11.9)\times 10^3$. The signal and background yields obtained from the fit are provided in Table~\ref{Tab:DsFitResults}. The results of the \Dsm\mup fit are validated by performing 1000 fits to pseudo-data. The data template in each pseudo-data fit is replaced with a \textit{toy template} generated by randomly selecting points from the fit templates. Consequently the yields of each fit component are known precisely. The fit templates used in the fits to pseudo-data are \textit{statistically compatible copies}, i.e. the contents of each bin is replaced by a random number sampled from a Gaussian distribution centred on the bin contents with width equal to the bin uncertainty. The \BsToDsMuNu yield in the pseudo-data is fixed at $197.9\times 10^3$ while the yields of all backgrounds are chosen by randomly sampling a Gaussian distribution centred on the yield determined from the fit and a width set to the component's uncertainty. The mean and width of the Gaussians are provided in Table~\ref{Tab:DsFitResults}. The distribution of the \BsToDsMuNu yield for all 1000 fits to the pseudo-data is plotted in Figure~\ref{fig:FitToys} alongside the pull distribution. The pull is defined as $(N_{\mathrm{Fit}}-N_{\mathrm{In}})/\sigma_{\mathrm{Fit}}$. where $N_{\mathrm{Fit}}$ and $\sigma_{\mathrm{Fit}}$ are the yield and uncertainty obtained from the fit to pseudo-data and $N_{\mathrm{In}}$ is the true number of \BsToDsMuNu events in the pseudo-data. The pulls should be centred at 0 and follow a Gaussian distribution with a width of 1 As seen in Figure~\ref{fig:FitToys} the pull distribution of the toy fits is well fit by a Gaussian with a which is slightly offset and has a width slightly less than 1 implying that the fit uncertainties are overestimated. A conservative approach is taken and the narrow width is ignored. The offset is treated as a systematic error and detailed later in Section~\ref{sec:SF_Systematics}. 

\begin{comment}
\begin{table}[htbp]
	\centering
	\scriptsize
	\begin{tabular}{ll|rrrrrrrr}
	                                  & Param.  &  1     &  2     &  3     & 4      & 5     & 6      & 7      & 8      \\\hline
		\B\to\Ds\D                    & 1       &  1.000 &  0.141 & -0.007 & -0.873 & 0.547 & -0.797 & -0.510 &  0.003 \\
		$f_{\Bs\to\Ds\D}$             & 2       &  0.141 &  1.000 &  0.006 &  0.013 &-0.028 & -0.015 & -0.032 & -0.001 \\
 		$f_{\Bs\to\Ds_1^*\mup\neum}$  & 3       & -0.007 &  0.006 &  1.000 & -0.049 & 0.066 & -0.009 & -0.014 &  0.003 \\
 		$\B\to\Ds_{0,1}^*$            & 4       & -0.873 &  0.013 & -0.049 &  1.000 &-0.775 &  0.534 &  0.632 & -0.002 \\
 		\Bs\to\Dssm\mup\neum X        & 5       &  0.547 & -0.028 &  0.066 & -0.775 & 1.000 & -0.313 & -0.908 & -0.010 \\
 		\Bs\to\Ds\taup\neu X          & 6       & -0.797 & -0.015 & -0.009 &  0.534 &-0.313 &  1.000 &  0.335 & -0.037 \\
 		\BsToDsMuNu                   & 7       & -0.510 & -0.032 & -0.014 &  0.632 &-0.908 &  0.335 &  1.000 & -0.004 \\
 		Fake \mup                     & 8       &  0.003 & -0.001 &  0.003 & -0.002 &-0.010 & -0.037 & -0.004 &  1.000 \\
	\end{tabular}
	
\end{table}


\end{comment}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.49\textwidth, page=2]{Fit/ToyPlots_Ds.pdf}
	\caption[\BsToDsMuNu toy fit pulls.]{A distribution of pulls from fits to 1000 pseudo datasets. The input \BsToDsMuNu yield is fixed for each toy fit and the variation in results is due to statistical uncertainties in the fit. %The pull is defined as the difference between the output value and he fit value divided by the uncertainty in the toy fit.
	}
	\label{fig:FitToys}
\end{figure}
