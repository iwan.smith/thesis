\section{Relative Efficiency Determinations and corrections}
\label{sec:EfficiencyCalcs}



\subsection{Generator Efficiency}
\label{ssec:GeneratorEff}


\begin{table}[hbt]
	\centering
	\begin{tabular}{l|llll}
	 & $\varepsilon_{\mathrm{Gen.}}~[\%]$ & $\sigma_{\mathrm{stat.}}~[\%]$ & $\sigma_{\mathrm{FF.}}~[\%]$  & $\sigma_{\mathrm{comb.}}~[\%]$\\\hline
	\BsToDsMuNu                                & 17.87    & 0.08    &&            \\                
	\BsToKMuNu                                 & 20.51    & 0.08    & 0.23 & 0.24 \\   
	$\BsToKMuNu_{~~\qsq<7~\mathrm{GeV^2/c^4}}$ & 19.67    & 0.12    & 0.03 & 0.12 \\
	$\BsToKMuNu_{~~\qsq>7~\mathrm{GeV^2/c^4}}$ & 20.96    & 0.11    & 0.16 & 0.19 \\

	\end{tabular}
	\caption[Efficiency of requiring final state partices are within detector acceptance.]{Generator efficiencies for \BsToKMuNu and \BsToDsMuNu in different \qsq regions. The uncertainties originate from Monte Carlo statistics, form factor parametrisation and are summed in quadrature. For \BsToDsMuNu there is negligible variation in Form Factors between parametrisations and the form factor uncertainty is ignored.}
	\label{tab:GenEffs}
\end{table}


A pre-selection is applied to Monte Carlo events before the simulation of particle interactions with the detector. These selections are called \textit{generator cuts} as they are applied immediately after the generation of the decay. A selection is made on the polar angle, $\theta_{\mathrm{flight}}$, 
\begin{equation}
	0.01 < \theta_{\mathrm{flight}} < 0.4,
\end{equation}
on some or all of the final state particles. When simulating \BsToKMuNu events, generator cuts are applied to all charged final state particles, and when simulating $\BsToDsMuNu X$ events the cuts are applied to the muon and daughters of the \Ds. For all other backgrounds the generator cuts are applied on all charged final state tracks. As this analysis measures \AbsVub with respect to a given \qsq selection the generator efficiency must be determined for that region rather than the whole sample. To measure the generator efficiencies for \BsToKMuNu and \BsToDsMuNu small Monte Carlo samples of 250,000 events are generated before generator cuts and detector simulation. The Generator efficiencies are plotted against \qsq in Figure~\ref{fig:GenEff} with the simulated \qsq distributions overlaid before and after the selection. The generator efficiencies are quoted in Table~\ref{tab:GenEffs}. The efficiencies for \BsToKMuNu vary with \qsq and a systematic error is assigned to the calculated efficiency taking into account the variations in efficiency due to variations in form factor parametrisations. The variation in efficiency is probed by reweighting the Monte Carlo to be consistent with the three form factor parametrisation from Lattice QCD and sum rules, and the systematic is taken as half the difference between the highest and lowest values of calculated efficiency.





\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.495\textwidth]{GenEff.pdf}
	\includegraphics[width=0.495\textwidth]{GenEff_Ds.pdf}
	\caption[Efficiency plots requiring final state particles are within the detector acceptance.]{The Generator efficiencies plotted against the true \qsq for \BsToKMuNu (left) and \BsToDsMuNu (right). The \qsq distributions for the signal Monte Carlo samples are plotted in grey before and after the selections are applied.}
	\label{fig:GenEff}
\end{figure}







\subsection{Particle Identification}
\label{ssec:ParticleID}

Particles are identified by combining information from the calorimeters, muon system and the ring-imaging Cherenkov (RICH) detectors providing excellent charged particle separation and rejection. The simulation does not accurately model the efficiency of selecting events using particle identification likelihoods and a data driven method is needed to correctly calculate the particle identification, PID, efficiencies. The PIDCalib package~\cite{Anderlini:2202412} calculates the efficiency of applying a PID selection on an arbitrary dataset using a tag and probe method to determine the true efficiencies of a selection. The calibration decays used to calculate the PID efficiencies are listed in Table~\ref{tab:PIDSamples}. The PID selections applied to data and simulation are listed in Table~\ref{tab:PIDSelection}. To minimise systematic effects, tight PID selections are only applied to the opposite sign kaon and muon while for \BsToDsMuNu, very soft selections are applied to the opposite sign \pim\Kp pair. Consequently the efficiency of PID selections will be similar for both the signal and normalisation decays and systematic effects are reduced when calculating corrections to the ratio of efficiencies.



\begin{table}[hbt]
\centering
	\begin{tabular}{l|ll}
	Decay                           & Tag                    & Probe     \\\hline
	$\Dstarp\to(\Dz\to\Km\pip)\pip$ & \textit{soft}~\pip      & \Km       \\
	$\Dstarp\to(\Dz\to\Km\pip)\pip$ & \textit{soft}~\pip      & \pip      \\
	Detached $\jpsi\to\mup\mun$     & $\mu^\pm$              & $\mu^\mp$ \\
	$\PLambda\to\proton\pim$        & \pim                   & \proton   \\
	\end{tabular}
	\caption[Decays used to calibrate particle identification efficiencies.]{The decays used to calibrate PID efficiencies. The low momentum (soft) tag \pip originates from the \Dstarp decay allowing the flavour of the \Dz to be unambiguously identified.}
	\label{tab:PIDSamples}
\end{table}


\begin{table}[hbt]
\centering
	\begin{tabular}{l|lll}
	\mup & $\mathcal{L}_{\mu/\pi} > 3~$&$\mathrm{and}~\mathcal{L}_{\mu/\proton} > 0$&$\mathrm{and}~\mathcal{L}_{\mu/K  } > 0$\\
	\Km  & $\mathcal{L}_{K  /\pi} > 5~$&$\mathrm{and}~\mathcal{L}_{K  /\proton} > 5$&$\mathrm{and}~\mathcal{L}_{K  /\mu} > 5$\\
	\Kp  & $\mathcal{L}_{K  /\pi} >-2 $ &&\\
	\pim & $\mathcal{L}_{K  /\pi} < 20$ &&\\
	\end{tabular}
	\caption[PID selections applied to final state particles.]{The PID likelihood selections applied to all particles. Selections are aligned between \BsToKMuNu and \BsToDsMuNu minimising systematics when taking the ratio of efficiencies.}
	\label{tab:PIDSelection}
\end{table}



The PID efficiencies are calculated using a \textit{fit and count} method, A fit is performed to the invariant mass distribution of the parent particle and \textit{sWeights} are calculated. The efficiency is taken as the ratio of the sum of the \textit{sWeights} before and after the PID selection. The PID efficiency varies with the kinematics of the track under consideration and the conditions of the underlying event, consequently differences in kinematics between the calibration sample and the signal sample could result in systematic differences in PID efficiency. To minimise systematic errors a lookup table binned in momentum, pseudorapidity and track multiplicity in the underlying events is generated with each entry containing the PID efficiency for that region of data. When choosing a binning scheme for the lookup table one must choose a binning scheme with a trade off between variance and bias. With a low number of  bins the statistics in each bin will be high ensuring a precise measurement of the efficiency however the intra-bin variation in efficiency will be higher resulting in a biased measurement. With a large number of bins the intra-bin variations in efficiency will be minimised however the statistics in each bin will be lower resulting in an efficiency measurement with greater variance. Additional systematic effects are introduced via the \textit{sWeight} procedure used to determine the yields of the calibration sample before and after PID selections in each bin. Variations in the shape of the signal peak or background distributions introduced by the application of a selection may result in the value obtained by summing the weights to differ from the yield under the signal peak resulting in unphysical values of the efficiency. When applying a loose PID selection it is not unusual to see quoted efficiencies greater than one, purely as a consequence of biases due to the \textit{sWeight}ing procedure. A more rigorous approach would be to perform many fits of the invariant mass distribution before and after the selection and take a ratio of the yields obtained from the two fits, although this approach requires significant human input to ensure the quality of all the fits and is not feasible.


\begin{figure}[htbp]
	\centering
	\begin{overpic}[width=0.495\textwidth]{PID/EffPID_K_Up_0_Data.pdf}
	\put(65,3){\colorbox{white}{\footnotesize Pseudorapidity}}
	\end{overpic}
	\begin{overpic}[width=0.495\textwidth]{PID/EffPID_K_Up_0_MC.pdf}
	\put(65,3){\colorbox{white}{\footnotesize Pseudorapidity}}
	\end{overpic}
	\caption[2D projection of kaon PID efficiencies]{A two dimensional projection of the PID efficiency lookup table for kaons determined from data (left) and Monte Carlo (right) ${\Dstarp\to\Dz\pip}$ decays. }
	\label{fig:PID_Table_K}
	\begin{overpic}[width=0.495\textwidth]{PID/EffPID_Mu_Up_0_Data.pdf}
	\put(65,3){\colorbox{white}{\footnotesize Pseudorapidity}}
	\end{overpic}
	\begin{overpic}[width=0.495\textwidth]{PID/EffPID_Mu_Up_0_MC.pdf}
	\put(65,3){\colorbox{white}{\footnotesize Pseudorapidity}}
	\end{overpic}
	\caption[2D projection of muon PID efficiencies]{A two dimensional projection of the PID efficiency lookup table for muons determined from data (left) and Monte Carlo (right) ${\jpsi\to\mup\mun}$ decays. }
	\label{fig:PID_Table_Mu}
\end{figure}


To minimise systematic effects from the intra bin variations in efficiency and the \textit{sWeight} background subtraction a MC/Data driven correction is used instead of the pure data driven correction. The data driven correction returns a \textit{true} efficiency value for a given PID cut, the MC/Data driven correction returns the ratio of PID efficiencies obtained from data and Monte Carlo. This ratio is used to correct the PID efficiency in the simulation. 



To determine the PID efficiencies in Monte Carlo, samples are generated corresponding to the decays ${\Dstarp\to\Dz\pip}$ and  ${\jpsi\to\mup\mun}$ with a detached secondary vertex. Differences in the kinematics between the simulated samples and data are corrected using the GBReweighter package~\cite{Rogozhnikov:2016bdp}. The target sample for the reweighting is the \textit{sWeight}ed data and the source sample is the Monte Carlo. As the target data set is weighted, the reweighting procedure has the effect of simultaneously correcting the kinematics and applying a weight mimicking the effects of the \textit{sWeights} to the simulated sample. The only remaining discrepancy between the simulation and data are the mismodelled PID distributions. The efficiency of a PID selection in data is determined by taking the ratio of the sum of \textit{sWeights} before and after a selection, and the efficiency in Monte Carlo is determined by taking the ratio of the sum of correcting weights before and after a selection. When comparing kinematically equal Monte Carlo and Data the intra bin variations in PID efficiency will be equal. Consequently when taking the ratio of efficiencies systematic effects from intra bin variations in efficiency cancel. This method relies on the assumption that the intra bin correction factor is constant. The calculated PID efficiencies binned in pseudorapidity and momentum for the muon and kaon are plotted in Figures~\ref{fig:PID_Table_Mu} and~\ref{fig:PID_Table_K} respectively for both data (left) and Monte Carlo (right).

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.495\textwidth, page=5]{PIDPlots_K.pdf}
	\includegraphics[width=0.495\textwidth, page=5]{PIDPlots_Ds.pdf}
	\caption[PID efficiencies plotted against corrected mass.]{The efficiencies of PID selections are plotted against the \Bs corrected mass for \BsToKMuNu (left) and \BsToDsMuNu (right). The signal corrected mass distribution is shaded in light grey.}
	\label{fig:PID_MCORR}
\end{figure}


When determining efficiencies form Monte Carlo each track from each event is weighted by the correction factor obtained from the lookup table. The corrected Monte Carlo yield is taken as the sum of the correction weights. Systematic uncertainties are quantified by performing 1000 pseudo-experiments, each time varying the contents of the lookup tables within the obtained errors. The PID corrections for each \qsq bin used in the fits is given in Table~\ref{tab:PIDCorrList}, and the PID efficiencies for Monte Carlo and data are plotted against the corrected mass in Figure~\ref{fig:PID_MCORR}.


\begin{table}[hbt]
\centering
	\setlength\tabcolsep{1 pt}
	\begin{tabular}{l|rclrclrcl}
			                                 & \multicolumn{3}{l}{\BsToKMuNu} \hspace*{3mm} & \multicolumn{3}{l}{\BsToDsMuNu}\hspace*{3mm}   & \multicolumn{3}{l}{Ratio} \\\hline
	No Sel.                                  & 0.855 &$\pm$& 0.004             & 0.823 &$\pm$& 0.014               & 1.039 &$\pm$& 0.015       \\
	$\qsq_{\Km\mup} < 7~\mathrm{GeV^2/c^4}$  & 0.850 &$\pm$& 0.006             &&&                                 & 1.033 &$\pm$& 0.015       \\
	$\qsq_{\Km\mup} > 7~\mathrm{GeV^2/c^4}$  & 0.863 &$\pm$& 0.002             &&&                                 & 1.048 &$\pm$& 0.016
	\end{tabular}
	\caption[PID efficiencies with systematic uncertainties.]{PID correction factors averaged over all tracks and all events applied to Monte Carlo. Corrected efficiencies are obtained by multiplying the Monte Carlo efficiency by the correction factor. Due to correlations between the uncertainties in the \BsToDsMuNu and \BsToKMuNu channels the uncertainty on the ratio is smaller than that obtained from a naive propagation of uncertainties. }
	\label{tab:PIDCorrList}
\end{table}




\subsection{Tracking Correction}
\label{ssec:TrackingCorr}


It is of vital importance that the efficiency of reconstructing tracks is well understood when performing a cross section or branching fraction measurement. The track reconstruction efficiency is is over 95\% and is determined from Monte Carlo. A data driven correction is applied to the simulation using clean ${\jpsi\to\mup\mun}$ decays. The tracking reconstruction efficiency is measured using a tag and probe method, the tag muon is fully reconstructed as well identified muon and the probe track is partially reconstructed without information from at least one subdetector which is being probed. The tracking efficiency is determined by counting the amount of fully reconstructed tracks correspond to the partially reconstructed probe track. Performing the tag and probe analysis on both simulation and data yields a discrepancy of approximately 2\%~\cite{Aaij:2014pwa}.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\textwidth]{TrackingTable.pdf}
	\caption[2D table of tracking efficiency corrections.]{The look-up table used to correct the tracking efficiency of charged tracks, binned in momentum and psedorapidity.}
	\label{fig:TrackingCorrectionTable}
\end{figure}

A lookup table of the ratio of tracking efficiencies of data and Monte carlo is provided by the \lhcb collaboration. 
The two dimensional table binned in momentum and pseudorapidity is visualised in Figure~\ref{fig:TrackingCorrectionTable}. The tracking efficiency corrections are applied as a weight on each track as determined from the lookup table and efficiencies are corrected by taking the product of the weights for each track. As \BsToKMuNu and \BsToDsMuNu contain two and four charged particles in their final states the uncertainties partially cancel when taking the ratio of the efficiencies.

The uncertainties on the overall correction factor are determined by performing 1000 pseudo-experiments, each time the efficiencies in the lookup table are varied within their uncertainties. The tracking corrections to the efficiency calculations are summarised in Table~\ref{Tab:TrackingCorr} for each of the \qsq bins used in the fits.

\begin{table}
	\setlength\tabcolsep{3 pt}
	\centering
	\begin{tabular}{l|rcl|rcl|rcl}
	\Km\mup~~\qsq Sel.                      & \multicolumn{3}{l|}{\BsToKMuNu}   & \multicolumn{3}{l|}{\BsToDsMuNu} & \multicolumn{3}{l}{Ratio} \\\hline
	No Sel.                                 & $1.007$ & $\pm$ &$0.001$          & $1.018$ & $\pm$ &$0.005$         & $0.990$ & $\pm$ &$0.004$ \\  
	$\qsq_{\Km\mup} < 7~\mathrm{GeV^2/c^4}$ & $1.006$ & $\pm$ &$0.001$          & $1.018$ & $\pm$ &$0.005$         & $0.989$ & $\pm$ &$0.004$ \\  
	$\qsq_{\Km\mup} > 7~\mathrm{GeV^2/c^4}$ & $1.010$ & $\pm$ &$0.002$          & $1.018$ & $\pm$ &$0.005$         & $0.992$ & $\pm$ &$0.004$ \\  
	\end{tabular}
	\caption{Tracking efficiency corrections applied to Monte Carlo events.}
	\label{Tab:TrackingCorr}
\end{table}




\subsection{\Bu\to\jpsi\Kp corrections}
\label{ssec:JPsiKCorr}

The decays \BsToKMuNu and \BsToDsMuNu are partially reconstructed due to the missing neutrino and have broad distributions making it difficult or impossible to isolate a pure signal sample in data. In order to validate the efficiencies of a selection and ensure that biases between data and simulation are corrected, the decay ${\Bu\to\jpsi\Kp}$ is used as a proxy for the signal decay. When reconstructed using only one muon the \Bu decay is kinematically very similar to the \BsToKMuNu decay, allowing the efficiencies of selections on kinematic variables to be validated. When fully reconstructed the efficiencies of selecting \Bu decays is similar to the signal \Bs decay as there are no additional tracks which can be associated with the secondary vertex.

\begin{table}[hbt]
	\centering
	\small
	\begin{tabular}{l|lll}
			                    & \Km\mup         & \Km\mup                       & \Dsm\mup        \\
			                    &                 & $\qsq > 7 \mathrm{GeV^2/c^4}$ &                 \\\hline
	$\sigma m_{\mathrm{Corr.}}$ & $1.02 \pm 0.02$ & $1.03\pm0.02$                 &                 \\
 	Isolation BDT               & $0.99 \pm 0.03$ & $1.00\pm0.01$                 & $0.989\pm0.014$ \\
 	Charged Track BDT           & $0.96 \pm 0.03$ & $0.96\pm0.03$                 &                 \\
 	Same Sign BDT               & $1.00 \pm 0.04$ & $0.95\pm0.04$                 &                 \\
	\end{tabular}
	\caption{Correction factors applied to Monte Carlo determined from simulated and real decays of ${\Bu\to\jpsi\Kp}$.}
	\label{tab:JPsiK_Correc}
\end{table}

Efficiency corrections are calculated for the corrected mass uncertainty cut and the BDT response variables. The efficiency of a selection is calculated for ${\Bu\to\jpsi\Kp}$ by performing a fit to the invariant mass distribution of the \mun\mup\Kp triad before and after a selection. The correction factor is the ratio of the efficiency in data and Monte Carlo and the efficiency for \BsToKMuNu obtained from Monte Carlo is scaled by the correction factor. An uncertainty obtained from the correction factor is applied as a systematic correction. The corrections are listed in Table~\ref{tab:JPsiK_Correc} and plots displaying \sPlot background subtracted ${\Bu\to\jpsi\Kp}$ data alongside Monte Carlo are given in Figures~\ref{fig:Correct_JPsiK_MCE}~-~\ref{fig:Correct_JPsiK_BDT}. The \qsq of ${\Bu\to\jpsi\Kp}$ peaks at $m_\jpsi^2$ resulting in very few events being reconstructed in the low \qsq bin. The corrections applied in the low \qsq are set equal to those in the high \qsq bin.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.6\textwidth, page=1]{EfficiencyCorrections.pdf}
	\caption[Corrected mass uncertainty calibration.]{The corrected mass uncertainty for ${\Bu\to\jpsi\Kp}$ (red) and \BsToKMuNu (black). A \sPlot background subtraction is performed on the data. The correction factor is taken as the ratio of ${\Bu\to\jpsi\Kp}$ data and Monte Carlo decays passing the selection. Rejected events are highlighted in the shaded region. Events rejected by the selection are in the shaded region.}
	\label{fig:Correct_JPsiK_MCE}
\end{figure}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.495\textwidth, page=2]{EfficiencyCorrections.pdf}
	\includegraphics[width=0.495\textwidth, page=5]{EfficiencyCorrections.pdf}
	\caption[Isolation BDT calibration plots.]{The response of the isolation BDT for \BsToKMuNu (left) and \BsToDsMuNu (right) is plotted against the ${\Bu\to\jpsi\Kp}$ calibration samples. }
	\label{fig:Correct_JPsiK_Isolation}
\end{figure}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.495\textwidth, page=3]{EfficiencyCorrections.pdf}
	\includegraphics[width=0.495\textwidth, page=4]{EfficiencyCorrections.pdf}
	\caption[Selection BDT calibration plots.]{The response of the BDTs rejecting charged (left) and same sign (right) backgrounds for \BsToKMuNu are plotted against the ${\Bu\to\jpsi\Kp}$ calibration samples. }
	\label{fig:Correct_JPsiK_BDT}
\end{figure}



\subsection{\qsq Migration}
\label{ssec:qsqmigration}


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.8\textwidth, page=2]{QsqMigration.pdf}
	\caption[Visualisation of \qsq migration.]{The reconstructed \qsq solution selected using the regression model is plotted against the true \qsq for simulated \BsToKMuNu events. The regions of inward and outward migration are shaded when requiring $\qsq > 7~\mathrm{GeV^2/c^4}$.}
	\label{fig:qsq_true_reco}
\end{figure}

Having selected a neutrino solution using the linear regression method detailed in Section~\ref{ssec:neutrinoReco} a selection is made. The resolution on the reconstructed \qsq will result in some migration of events across the selection boundary with some events rejected that should have been selected and vice versa. The distribution of the true \qsq from Monte Carlo is plotted against the reconstructed \qsq in Figure~\ref{fig:qsq_true_reco}, the region containing events migrating either in or out of the high \qsq region are illustrated. Inward migration is defined by the events with a true \qsq outside the region of interest but are reconstructed inside due to the resolution. Outward migration is defined by the events which are truly in the region of interest but are reconstructed out. A correction factor is calculated from simulated Monte Carlo events by taking the ratio of events truly in the high \qsq with events reconstructed in the \qsq region. The Monte Carlo is reweighted to be consistent with form factor predictions from Lattice QCD and light cone sum rules, and the percentages of events migrating in and out are listed in Table~\ref{tab:qsqmig}. As the correction factor is dependant on the form factor modelling, a systematic uncertainty is assigned to the correction factor, taken as the standard deviation of the correction factors for all form factor predictions. The correction factor is taken as the mean value for all form factor models. The migration corrections and systematic uncertainties are found to be:
\begin{equation}
	\begin{aligned}
		\mathrm{Corr.~Mig.}_{\qsq < 7 \mathrm{GeV^2/c^4}} &= 1.002 \pm 0.008\\
		\mathrm{Corr.~Mig.}_{\qsq > 7 \mathrm{GeV^2/c^4}} &= 0.996 \pm 0.009\\
	\end{aligned}
\end{equation}






\begin{table}
\centering
	\begin{tabular}{l|ccc}
		Model     & Migration in [\%]  & Migration out [\%] & Correction \\\hline
		ISGW2     & 4.15               & 4.00               & 0.996      \\
		K\&R      & 3.89               & 4.03               & 0.994      \\
		Bouchard  & 3.97               & 3.73               & 0.994      \\
		Flynn     & 3.49               & 4.34               & 1.02
	\end{tabular}
	\caption[\qsq bin migration correction factors.]{Correction factors to the efficiency for migration in and out of the high \qsq region due to resolution on the reconstructed \qsq using the choice closest to the regression value. Results obtained from simulated \BsToKMuNu events after a full selection is applied. The events have been reweighted to be consistent with predictions from Lattice QCD and LCSR. The Migration in is defined as the percentage of events with true \qsq below 7~$\mathrm{GeV^2/c^4}$ and reconstructed \qsq above 7~$\mathrm{GeV^2/c^4}$.}
	\label{tab:qsqmig}
\end{table}



\subsection{Final Corrected Relative Efficiency}
\label{ssec:FinalEffRel}

\begin{table}
\centering
	\begin{tabular}{l|l}
	No \qsq sel.                  & 10.2 \% \\
	$\qsq < 7\mathrm{\gev^2/c^4}$ & 19.9 \% \\
	$\qsq > 7\mathrm{\gev^2/c^4}$ & 3.41 \% \\
	\end{tabular}
	\caption[Efficiency uncertainty from form factor uncertainty]{The relative uncertainty on the \BsToKMuNu selection efficiency originating from a lack of knowledge on the \qsq distribution.}
	\label{Tab:Err_FF}
\end{table}

The efficiencies and corrections used to determine the full selection efficiency of \BsToKMuNu and \BsToDsMuNu are listed in Table~\ref{Tab:EffSummary}. The uncertainties on the corrections are taken as systematic uncertainties when calculating the final ratio of branching fractions. The corrected efficiency is plotted against the true \qsq in Figure~\ref{fig:EvvVsqsq} for \BsToKMuNu (left) and \BsToDsMuNu (right). The unfortunately large bias on the efficiency of the signal mode combined with a lack of knowledge on the shape of true \qsq distribution results in the assignment of a systematic uncertainty on the final corrected efficiency. The systematic uncertainty on the final corrected efficiency originating from an uncertainty on the knowledge of the \qsq distribution is determined by calculating the corrected \BsToKMuNu efficiency under each of the four form factor models and taking the standard deviation. As seen in Figure~\ref{fig:FF_Gamma_K} the dominant factor contributing to the true \qsq distribution at low \qsq is the form factor parametrisation, while at high \qsq the dominant contribution comes from phase space. The systematic uncertainties originating from a lack of knowledge on the true \qsq distribution will therefore be greater at low \qsq, and the systematic uncertainties are listed in Table~\ref{Tab:Err_FF}. The full summary of systematic uncertainties is given in Table~\ref{Tab:Err_Syst_Summary}.


\begin{table}[hb]
\centering
\footnotesize
	\begin{tabular}{l|l|lll}
		Source                         & Efficiency [\%]     & \multicolumn{3}{l}{\BsToKMuNu}                                                   \\
		                               & \BsToDsMuNu         &                     & $\qsq < 7\mathrm{GeV^2/c^4}$ & $\qsq > 7\mathrm{GeV^2/c^4}$\\ \hline
		Generator                      & $17.87\pm0.08$      & $20.5\pm0.2$        & $19.7\pm0.1$                 & $21.0\pm0.2$                \\
        Selection                      & $0.62$              & $0.422$             & $0.504$                      & $0.217$                     \\
        \hline
        \multicolumn{1}{l}{Source}                         & \multicolumn{4}{l}{Correction}\\ \hline

        Tracking                       & $1.018\pm0.004$     & $1.007\pm0.001$     & $1.006\pm0.001$              & $1.010\pm0.002$             \\ 
        PID.                           & $0.823\pm0.085$     & $0.855\pm 0.017$    & $0.850\pm0.025$              & $0.863\pm0.006$             \\
        $\sigma_{m_{\mathrm{corr.}}}$  &                     & $1.02\pm0.02$       & $0.91 \pm0.14$               & $1.026\pm0.002$             \\
        Isolation                      & $0.989\pm0.014$     & $0.993\pm0.033$     & $0.995\pm0.013$              & $0.995\pm0.013$             \\
        Charged BDT                    &                     & $0.966\pm0.034$     & $0.959\pm0.029$              & $0.959\pm0.029$             \\
        Same sign BDT                  &                     & $0.995\pm0.034$     & $0.948\pm $                  & $0.948\pm0.041$             \\
        \qsq migration                 &                     &                     & $1.002\pm0.008$              & $0.996\pm0.009$             \\

        \hline
        \multicolumn{1}{l}{}           & \multicolumn{4}{l}{Corrected Efficiency [\%]}\\ \hline
                                       & $0.109 \pm 0.011 $  & $0.084 \pm 0.011$  & $0.082 \pm 0.021$  & $0.0456\pm 0.0032$    \\

 
	\end{tabular}
	\caption[Summary of efficiencies and corrections]{Summary of efficiencies and corrections entering into the combined efficiency for the \BsToKMuNu and \BsToDsMuNu modes.}
	\label{Tab:EffSummary}
\end{table}


\begin{figure}[hb]
	\centering
	\includegraphics[width=0.495\textwidth, page=1]{Efficiencies.pdf}
	\includegraphics[width=0.495\textwidth, page=2]{Efficiencies.pdf}
	\caption[Efficiency of selection plotted against \qsq.]{The corrected Efficiencies for successive selections on \BsToKMuNu (left) and \BsToDsMuNu (right) candidates are  plotted against the true \qsq. }
	\label{fig:EvvVsqsq}
\end{figure}


The final corrected ratio of efficiencies for \BsToKMuNu and \BsToDsMuNu are listed in Table~\ref{Tab:FinalEff}.

\begin{table}
	\centering
	\begin{tabular}{l|l}
										& $\varepsilon_{\mathrm{rel}}$   \\\hline

		No \qsq sel.                    & $0.671 \pm 0.056$    \\
	    $\qsq < 7\mathrm{GeV^2/c^4}$    & $0.682 \pm 0.115$    \\
	    $\qsq > 7\mathrm{GeV^2/c^4}$    & $0.356 \pm 0.027$    \\
	\end{tabular}
	\caption[Final corrected relative efficiency]{Final corrected efficiency ratio, $\varepsilon_{\BsToKMuNu}/\varepsilon_{\BsToDsMuNu}$, for the signal and normalisation channels within each region of \qsq.}
	\label{Tab:FinalEff}
\end{table}




%\subsection{Final Systematic Uncertainties}
%\label{ssec:FinalSystematics}


\begin{table}
	\begin{tabular}{l|c|ccc}
	%                                       & \multicolumn{4}{l}{Relative uncertainty [\%]} \\
	Uncertainty [\%]                       & \BsToDsMuNu       & \multicolumn{3}{l}{\BsToKMuNu} \\
	                                       &                   & No \qsq sel. & $\qsq<7$            & $\qsq>7$ \\\hline
	$\BF(\Dsm\to\Km\Kp\pim)$               &3.3                &              &\\
	Form factor uncertainty                &3.1                &              &9.7                  & 22.45\\
	Tracking                               &0.41               &0.15          &0.15                 & 0.16\\
	Particle Identification                &10.2               &2.0           &3.0                  & 0.74\\
	$m_\mathrm{corr}$ error                &                   &2.0           &2.0                  & 2.0\\
	Isolation                              &1.4                &3.3           &1.3                  & 1.3\\
	Charged BDT                            &                   &3.5           &3.0                  & 3.0\\
	Same Sign BDT                          &                   &4.9           &4.3                  & 4.3\\
	\qsq migration                         &                   &              &0.85                 & 0.90\\
	$\varepsilon$ generator                &0.08               &0.24          &0.12                 & 0.19\\
	$\varepsilon$ error from FF.           &                   &10.2          &19.9                 & 3.4\\
	\Bu\to\jpsi\Kp reco.                   &                   &2.1           &0.61                 & 3.8\\
	Fit Bias                               &0.59               &1.2           &4.6                  & 8.1\\ 
	$m_\mathrm{corr}$ template             &                   &1.4           &3.6                  & 0.87\\
	\end{tabular}
	\caption[Summary of systematic uncertainties]{Systematic uncertainties on the evaluated yields at production for \BsToDsMuNu and \BsToKMuNu. When taking the ratio of branching fractions some of the systematic uncertainties will partially cancel, and when calculating the ratio of {$|\Vub|/|\Vcb|$} the uncertainties will be approximately halved.}
	\label{Tab:Err_Syst_Summary}
\end{table}
