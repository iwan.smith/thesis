\section{Signal Fit}
\label{sec:SignalFit}

A two stage fit is is used to determine the signal and background yields in the \Km\mup data. The results of the first fit are used to calculate the \BsToKMuNu branching fraction and the second fit uses the results of the first as a constraint with the results used to measure $|\Vub|/|\Vcb|$ in two bins of \qsq. An initial fit is performed with no selection on the \qsq of the \Bs candidate and uses a corrected mass range of {$2500 \mevcc < m_{\mathrm{corr}} < 5700 \mevcc$}. Sections~\ref{ssec:ParticleID} onwards discuss the corrections applied to Monte Carlo simulation to correct the selection efficiencies. It is important to note that when producing input histograms for the fit the entries in each bin are weighted by kinematic, PID and tracking corrections detailed in Sections~\ref{subsec:KinematicCorrections},~\ref{ssec:ParticleID} and ~\ref{ssec:TrackingCorr}.

The purpose of the first fit is to accurately determine the \Bs yield in order to measure the ratio of branching fractions.
The second fit requires the \qsq solution to be valid, reducing the fit range to {$2500 \mevcc < m_{\mathrm{corr}} < m_\Bs$, and a simultaneous fit is performed in two bins of \qsq, with the bin boundary placed at $\qsq = 7 \mathrm{GeV^2/c^4}$. The combined yields of the fits to the high and low \qsq samples are constrained to the values obtained from the first fit, 
\begin{equation}
	N_{\qsq<7} + N_{\qsq>7} = N \times \varepsilon_{\qsq>0},
\end{equation}
where $N_{\qsq\gtrless x}$ is the yield given a \qsq selection, $N$ is the yield with no selection and $\varepsilon_{\qsq>0}$ is the efficiency of requiring a \qsq selection. $N_{\qsq\gtrless x}$ is determined from the second fit, $N$ is determined from the first fit, and $\varepsilon_{\qsq>0}$ is determined from Monte Carlo.

The purpose of the second fit is to take our pre-existing knowledge of the yields obtained from the first fit and precisely determine the fractions of the yields in the high and low \qsq bins, thus allowing for a measurement of \AbsVub to be performed.

The dominant backgrounds in the fits to extract the \BsToKMuNu yields include decays from the excited \Kstar resonances, many  \bquark\to\cquark decays, combinatoric combinations of a kaon and muon, and candidates containing misidentified kaons. The most concerning background is the partially reconstructed decay {$\Bu\to\jpsi\Kp$}, which has a fit distribution almost identical to signal. The yields of many backgrounds may be constrained using data driven methods; the yields of misidentified kaons are constrained by measuring the efficiency of selecting/vetoing misidentified particles using a calibration sample. While the selections used in this analysis reject almost all reconstructible {\Bu\to\jpsi\Kp} candidates, there is still a significant contribution of decays where the additional muon falls outside the acceptance of the detector. Using a combination of a \jpsi mass constraint and the geometry of the decay, the \Bu peak is reconstructed and a fit is performed allowing the true yield of {\Bu\to\jpsi\Kp} events to be determined.



\subsection{Components and Templates}
\label{ssec:SignalFitComponents}


The signal fit extracts the \BsToKMuNu yield, separating it from a variety of backgrounds. The components and data sources used to generate fit templates are summarised in Table~\ref{tab:SigFitComponents}. The templates used in the fits are one dimensional histograms of the corrected \Km\mup mass, a binning scheme is chosen with variable bin widths, the bin boundaries chosen such that the number of candidates in each bin is approximately equal. When fitting to the sample with no selection on \qsq the template contains 30 bins and covers a range $2500 < m_\mathrm{corr} [\mev/c^2]< 5750$. When fitting to the high and low \qsq regions the templates contain 25 and 20 bins respectively, in both cases the fit range is $2500 < m_\mathrm{corr} [\mev/c^2]< m_\Bs$.
 The candidates for the template input histograms originate from a variety of sources including simulated Monte Carlo decays and background enriched data. The fit templates are summarised in Table~\ref{tab:SigFitComponents} along with the source of data used in the creation of the templates. Events originating from Simulated Monte Carlo decays are weighted to correct for differences in kinematics and mismodelling between the simulation and data.

Three excited resonances of the kaon are considered as backgrounds to the \BsToKMuNu decay, the $\Kstarm(892)$, $K^{*-}_2(1430)$ and $K^{*-}_0(1430)$. The background templates corresponding to the excited resonances of the kaon are combined into a single template with equal contributions. The merging of templates is motivated by a lack of knowledge on the relative branching fractions of the different decays and in part by the similarity of the template shapes. As the template shapes of \Kstarm decays are almost identical it is impossible to distinguish between the different excited resonances, therefore the {\Bs\to\Kstarm\mup\neum} decay is taken to mean the combination of all excited \Kstarm decays. The corrected \Km\mup mass for each template and the combination is plotted in Figure~\ref{fig:KMuNuCombinations} (left). 

To aid in plotting, the templates containing candidates with a misidentified particle are combined into a single template. As the yields of misidentified particles are determined externally to the signal fit in Section~\ref{ssec:MisIDConstr} and constrained, the merging of templates has no impact on the overall quality of the fit. The corrected \Km\mup distributions for the misidentified particles are plotted in Figure~\ref{fig:KMuNuCombinations} (right) with the combined template.







\begin{table}[hbt]
\centering
	\begin{tabular}{rl| l }
	\multicolumn{2}{c|}{Component}    & Source  \\\hline

	Signal&\BsToKMuNu                  & Monte Carlo \\
	&\Bs\to\Kstarm\mup\neum      & Monte Carlo \\
	&\Bu\to\jpsi\Kp              & Monte Carlo \\
	&\Bu\to\jpsi\phiz            & Monte Carlo \\
	&Combinatorics               & \textit{Mixing} of \Km and \mup from different events\\
	&Misidentified particles     & Signal candidates in data failing PID selections\\
	&\bquark\to\cquark\to\squark & Monte Carlo\\

	\end{tabular}
	\caption[Signal fit template data sources.]{Sources of data used to generate the corrected mass histograms for each \BsToKMuNu fit component.}
	\label{tab:SigFitComponents}
\end{table}





\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.49\textwidth, page=3]{Fit/KMuNuTemplates.pdf}
	\includegraphics[width=0.49\textwidth, page=6]{Fit/KMuNuTemplates.pdf}
	\caption[Merging of \Kstar templates and misidentified particle templates.]{The three \Bs\to\Kstarm\mup\neum templates (left) are combined into a single template with all contributions given an equal weight. The three sources of misidentified kaons are combined into a single template (right) with the yields for each template determined using the PIDCalib package.}
	\label{fig:KMuNuCombinations}
\end{figure}




\subsection{\Bu\to\jpsi\Kp Yield Constraint}
\label{ssec:Bu_JPsiKConstr}

The ${\Bu\to\jpsi\Kp}$ background is very effectively removed through the use of charged isolation and BDTs. These selections, however, are only effective when the additional muon is reconstructible, which is often not the case. The most likely case occurring when the muon is produced outside the acceptance of the detector. ${\Bu\to\jpsi\Kp}$ decays with a non reconstructible muon present a major concern, as the efficiency of selecting events  is similar to that of signal decays and shape of the reconstructed corrected mass is almost identical to the signal shape.

Using an approach similar to that detailed in Section~\ref{ssec:neutrinoReco} it is possible to reconstruct a \Bu mass peak. The momentum of the invisible muon perpendicular to the \Bu flight direction, \pperp, must be equal and opposite the momentum of the visible particles. The momentum of the invisible muon parallel to the \Bu flight direction, or longitudinal momentum, \plong, may be found from a knowledge of the \jpsi mass. When calculating the \jpsi mass from the \Kp\mup\mun four vectors, the only unknown component is the longitudinal momentum of the invisible muon. Solving the four vector equation yields \plong with a two fold ambiguity, 
\begin{equation}
	\begin{aligned}
		\pperp(\mun) &= - \pperp(\Km\mup),\\
		\plong(\mun) &= \frac{\pm\sqrt{A^2 + BC^2 - B}-AC}{C^2-1},
	\end{aligned}
	\label{eq:mu_reco_nu}
\end{equation}
where,
\begin{equation}
	\begin{aligned}
	A &= \frac{m_\jpsi^2 - m^2_\mu + \pperp^2(K^{-}) + E^2(\mup) + \plong^2(\mup) - \pperp^2(\mup\Km)}{2 E(\mup)}, \\
	B &= m_\mu^2 + \pperp^2(\mup\Km),\\
	C &= \frac{\plong(\mup)}{E(\mup)}.\\
	\end{aligned}
\end{equation}
This method of reconstructing the \Bu peak will be referred to as \textit{the neutrino method}\footnote{Using a mass constraint in combination with momentum asymmetries has traditionally been used to reconstruct neutrinos.}. Due to an imperfect vertex resolution approximately 15\% of events have unphysical solutions for \plong, i.e. when $A^2+BC^2 - B < 0$.


The ${\Bu\to\jpsi\Kp}$ yield is obtained by performing a binned maximum likelihood fit to a histogram containing both solutions of the \Bu invariant mass. The signal peak is modelled using a double Gaussian and the background shape is modelled with the sum of a Gaussian and a Crystal Ball function. The Crystal Ball function consists of a Gaussian in the central region with a power-law end tail~\cite{Gaiser:1982yw}. The function is given by:
\begin{equation}
	f(x;\alpha, n, \overline{x}, \sigma) = N \cdot 
	\begin{cases}
		\begin{aligned}
			&\exp(-\frac{(x-\overline{x})^2}{2\sigma^2}),     \hspace*{2mm}&\mathrm{for}\hspace*{2mm} \frac{x-\overline{x}}{\sigma} > -\alpha \\
			&A\cdot(B-\frac{(x-\overline{x})}{\sigma})^{-n}, \hspace*{2mm}&\mathrm{for}\hspace*{2mm} \frac{x-\overline{x}}{\sigma} \leq -\alpha
		\end{aligned}
	\end{cases}
\end{equation}
where,
\begin{equation}
	\begin{aligned}
		A &= \left(\frac{n}{|\alpha|}\right)^n\cdot \exp\left(-\frac{|\alpha|^2}{2}\right),     \hspace*{2cm}  &B &= \frac{n}{|\alpha|}-|\alpha| \\
		C &= \frac{n}{|\alpha|}\cdot \frac{1}{n-1}\cdot \exp\left(-\frac{|\alpha|^2}{2}\right), \hfill         &N &= \frac{1}{\sigma(C+D)},      \\
		D &= \sqrt{\frac{\pi}{2}}\left(1+\mathrm{erf}\left(\frac{|\alpha|}{\sqrt{2}}\right)\right).
	\end{aligned}	
\end{equation}


\begin{table}
	\centering
	\setlength\tabcolsep{3 pt}
	\hspace*{-5mm}\begin{tabular}{ll|rcl|rcl|rcl|rcl}
		&      & \multicolumn{3}{l|}{(1) Full Sel.}& \multicolumn{3}{l|}{(2) No BDT Iso.}& \multicolumn{3}{l|}{(3) No BDT \sPlot}& \multicolumn{3}{l}{$\varepsilon_{\mathrm{Reco.}}~[\%]$}\\\hline

		No \qsq Sel.         & M.C. & ~$2257$ &$\pm$ & $51$ ~   & ~$127800$ & $\pm$ & $400$~  &~$105800$&$\pm$ & $400$~   &   ~$82.8$& $\pm$ & $0.4$            \\
		                     & Data & ~$2220$ &$\pm$ & $100$~   & ~$ 78700$ & $\pm$ & $500$~  &~$ 58400$&$\pm$ & $300$~   &   ~$74.2$& $\pm$ & $0.6$            \\\hline

		$\qsq < 7\gev^2/c^4$ & M.C. & ~$217 $ &$\pm$ & $15$ ~   & ~$  8700$ & $\pm$ & $100$~  &~$  7470$&$\pm$ & $130$~   &   ~$85.9$ & $\pm$ & $1.8$           \\
		                     & Data & ~$270 $ &$\pm$ & $33$ ~   & ~$  5100$ & $\pm$ & $100$~  &~$  4290$&$\pm$ & $ 70$~   &   ~$84.1$ & $\pm$ & $2.1$           \\\hline

		$\qsq > 7\gev^2/c^4$ & M.C. & ~$2116$ &$\pm$ & $50$ ~   & ~$112800$ & $\pm$ & $400$~  &~$ 96900$&$\pm$ & $400$~   &   ~$85.9$ & $\pm$ & $0.6$            \\
		                     & Data & ~$2104$ &$\pm$ & $100$~   & ~$ 69800$ & $\pm$ & $400$~  &~$ 54500$&$\pm$ & $300$~   &   ~$78.1$ & $\pm$ & $0.6$            \\
	\end{tabular}
	\caption[\Bu\to\jpsi\Kp yields used as constraints in the signal fit.]{The ${\Bu\to\jpsi\Kp}$ yields obtained from a maximum likelihood fit to the \Bu invariant mass. Fits are performed to the \Bu invariant mass, 1, after a full selection using \textit{the neutrino method}, 2, before BDT selections using the least isolated track and 3, before BDT selections using the neutrino method with a \sPlot background subtraction performed from the results of fit 2. The efficiency is the ratio of events from fit 3 and fit 2.  }
	\label{tab:JPsiKConstraints}
\end{table}




\begin{figure}[htbp]
	\centering
\begin{overpic}[width=0.48\textwidth]{JPsiKPeak_Default_Low_Q2_Data.pdf} \put(60,65){\scriptsize $\qsq<7\gev^2/c^4$}\put(25,60){\large Data}\end{overpic}
\begin{overpic}[width=0.48\textwidth]{JPsiKPeak_Default_High_Q2_Data.pdf}\put(60,65){\scriptsize $\qsq>7\gev^2/c^4$}\put(25,60){\large Data}\end{overpic}\\
\begin{overpic}[width=0.48\textwidth]{JPsiKPeak_Default_Low_Q2_MC.pdf}   \put(60,65){\scriptsize $\qsq<7\gev^2/c^4$}\put(25,60){\large MC}\end{overpic}
\begin{overpic}[width=0.48\textwidth]{JPsiKPeak_Default_High_Q2_MC.pdf}  \put(60,65){\scriptsize $\qsq>7\gev^2/c^4$}\put(25,60){\large MC}\end{overpic}
	\caption[\Bu\to\jpsi\Kp invariant mass distributions and fits.]{The \Km\mup\mun invariant mass reconstructed from a \Km\mup pair using a knowledge of the \B flight direction. Fit results are plotted for Monte Carlo and data in both \qsq bins. The Monte Carlo background is from the incorrect muon solution.}
	\label{fig:Bu_JPsiK_Constr}
\end{figure}



The fit is performed in a two stage process. An initial fit is performed to the Monte Carlo distribution to determine the signal shape and a second fit is performed on the data distribution to determine the signal and background yields. The signal shape is fixed when fitting the data using the results from the Monte Carlo fit. Uncertainties in the signal and background shape make up the dominant systematic uncertainty when determining the {$\Bu\to\jpsi\Kp$} yield. To determine the true ${\Bu\to\jpsi\Kp}$ yield independent of the additional muon the fit results must be divided by the efficiency of reconstructing the additional muon. Before applying BDT selections a \Bu peak may be reconstructed by calculating the invariant mass of the \BsToKMuNu candidate combined with the least isolated track providing a relatively pure sample of \Bu candidates. The efficiency of reconstructing the additional muon using \textit{the neutrino method} is determined by measuring the \Bu yield reconstructed using Equation~\ref{eq:mu_reco_nu} given that the candidate truly originates from a \Bu. A fit is performed to the \Bu invariant mass obtained from the least isolated track from which a \sPlot background subtraction is performed. By plotting the \Bu invariant mass distribution calculated using \textit{the neutrino method} with the \sPlot background subtraction applied, the ratio of \Bu yields gives the efficiency of reconstruction. The results of fits to the \Bu invariant mass are given in Table~\ref{tab:JPsiKConstraints}. The \Bu invariant mass calculated using \textit{the neutrino method} is plotted in Figure~\ref{fig:Bu_JPsiK_Constr} for the high and low \qsq bins. Maximum likelihood fits to the \Bu invariant mass distributions are used to obtain the {\Bu\to\jpsi\Kp} yield in data after applying a full selection, and the yields are divided by the efficiencies quoted in Table~\ref{tab:JPsiKConstraints}. A discrepancy between Monte Carlo and data of $\approx 8\%$ in the measured reconstruction efficiency is applied as a systematic. When performing a fit to determine the \BsToKMuNu yields the ${\Bu\to\jpsi\Kp}$ yield is constrained using a Gaussian constraint centred on the yield with a width set tot he statistical error given in Table~\ref{tab:JPsiFitConstr}.

\begin{table}[hbt]
	\centering
	\begin{tabular}{l|l|l|l}
			                 & \Bu Yield  & Statistical & Systematic \\\hline
		No \qsq              & 2680       & 22         & 215        \\
		$\qsq < 7\mathrm{Gev^2/c^4}$ & 314        & 39         & 22         \\
		$\qsq > 7\mathrm{Gev^2/c^4}$ & 2450       & 20         & 195    
	\end{tabular}
	\caption[\Bu\to\jpsi\Kp yield constraints and systematic uncertainties.]{The yields, constraints and systematic uncertainties for the {$\Bu\to\jpsi\Kp$} yield used in fits to determine the \BsToKMuNu yield. The constraint applied to the \BsToKMuNu fit originates from the statistical uncertainty of the fits and the systematic originates from Monte Carlo discrepancies.}
	\label{tab:JPsiFitConstr}
\end{table}



\subsection{Misidentified Particle Yield Constraints}
\label{ssec:MisIDConstr}

\begin{table}
	\centering
	\begin{tabular}{l|lll}
	         & Likelihood Selection                                                                                     & MisID Rate $[\%]$ & Efficiency $[\%]$ \\\hline
	\Kpm     & $\mathcal{L}_{K/\pi} > 5~\mathrm{and}~ \mathcal{L}_{K/\proton}> 5~\mathrm{and}~ \mathcal{L}_{K/\mu} > 5$ & N/A               & 50.8              \\
	\pipm    & $\mathcal{L}_{K/\pi} < 0~\mathrm{and}~ \mathcal{L}_{\proton/\pi}< 0~\mathrm{and}~ \mathcal{L}_{\mu/\pi} < 0$ & 0.975             & 71.38             \\
	\proton  & $\mathcal{L}_{K/\pi} < 5~\mathrm{and}~ \mathcal{L}_{\proton/\pi}> 0~\mathrm{and}~ \mathcal{L}_{\mu/\pi} < 0$ & 1.446             & 29.8              \\
	\mupm    & $\mathcal{L}_{K/\pi} < 0~\mathrm{and}~ \mathcal{L}_{\proton/\pi}< 0~\mathrm{and}~ \mathcal{L}_{\mu/\pi} > 0$ & 0.325             & 77.5              \\
	\end{tabular}
	\caption
		[PID enrichment selections and MisID rates.]
		{The likelihood selection used to enrich the Fake Kaon sample with the desired particle type is given. The MisID rate is defined as the percentage of particles passing the kaon likelihood selection and the efficiency is defined as the percentage of particles passing the likelihood selection.}
	\label{tab:MisPID_Selections}
\end{table}


Despite tight selections on the likelihood criteria of the candidate kaon, some protons, pions and muons will pass the selections and be falsely reconstructed as kaons. The yields and fit distributions of particles misidentified as kaons must be determined. The rate of misidentification as a muon is considerably lower than that of the kaon, thus contributions from fake muons are not considered in the fit. A fake kaon is any particle falsely reconstructed as a kaon. A misidentified particle refers to a particle which has been misidentified, e.g. a misidentified pion is truly a pion which has been identified as e.g. a kaon. 

This section details the procedure used to determine the yields of each misidentified particle type. A dedicated stripping line is written with selections identical to the \BsToKMuNu line, except the likelihood criteria on the kaon is removed. A prescale\footnote{A random scaling used to reduce the rate. Discussed in Section~\ref{ssec:CutPresel}} of 0.02 is applied. It is implied that all yields with prescales have been correctly scaled. This is referred to as the fake kaon sample. A full selection is applied to the fake kaon sample. The kaon candidates in data are a blend of misidentified particles and true kaons. One may produce background samples containing misidentified particles with a high purity by simultaneous requiring that the candidate kaon has a low kaon likelihood, $\mathcal{L}_\Kpm$,  and a high likelihood for the desired particle under investigation. When searching for misidentified {$\Lb\to\proton\mun\neumb$} decays, a sample of high purity protons misidentified as kaons may be created by requiring the candidate kaon has a low kaon likelihood and a high proton likelihood.

The selections used to produce enriched samples of the different particle types are listed in Table~\ref{tab:MisPID_Selections} with the rates of misidentification and efficiency of the enrichment selection. The misidentification rates and efficiency of selection are calculated using the PIDCalib package~\cite{Anderlini:2202412}.

The yields of events passing the enrichment selections are listed in Table~\ref{tab:MisPID_Rates} alongside the scaling used to convert the enriched yield into the yield in data. The uncertainties quoted for the data yields originate from the limited yields in the fake muon sample. When performing a fit to the \Km\mup corrected mass a Gaussian constrained is applied to the yields of the fake samples with a mean at the derived data yield and a width equal to the uncertainty. The template shapes for the misidentified kaons are plotted in Figure~\ref{fig:KMuNuCombinations}. During plotting of fit results the templates for misidentified particles are merged into a single template. 
\begin{table}
\centering
\begin{comment}
	\begin{tabular}{l|lllll}
	         & Enriched Yield  & Scaling  & Data Yield   &  Data Yield  & Data Yield   \\
	         &                 &          &              &  $\qsq<7$    & $\qsq>7$     \\\hline
	\pipm    & 55650           & 0.0137   & $762\pm 23$  &  $496\pm18$  & $243\pm13$   \\
	\proton  & 18800           & 0.0485   & $911\pm 47$  &  $320\pm28$  & $512\pm35$   \\
	\mupm    & 28900           & 0.0042   & $121\pm 5.0$ &  $86\pm4$    & $33\pm3$     \\
	\end{tabular}
\end{comment}
	\begin{tabular}{l|lllll}
	         & Enriched \textit{Yield}  & Scaling  & Data \textit{Yield}   &  Data \textit{Yield}  & Data \textit{Yield}   \\
	         &                 &          &              &  $\qsq<7$    & $\qsq>7$     \\\hline
	\pipm    & 55650           & 0.0137   & $762\pm 23$  &  $496\pm18$  & $243\pm13$   \\
	\proton  & 18800           & 0.0485   & $911\pm 47$  &  $320\pm28$  & $512\pm35$   \\
	\mupm    & 28900           & 0.0042   & $121\pm 5.0$ &  $86\pm4$    & $33\pm3$     \\
	\end{tabular}
	\caption
		[MisID yields in enriched region and data.]
		{The yields of particles within the enriched regions selected using the selection in Table~\ref{tab:MisPID_Selections}. The Data Yield is the yield of misidentified particles passing the full selection. The scaling converts the enriched yield to the data yield, and is the ratio of columns two and three in Table~\ref{tab:MisPID_Selections}. The dominant uncertainty on the data yields originates from the limited statistics in the enriched sample.}
		\label{tab:MisPID_Rates}
\end{table}


\subsection{Fit Model}
\label{ssec:SF_Model}

The same Beeston Barlow fit method detailed in Section~\ref{sec:FitMethodBB} and used to determine the \BsToDsMuNu yield is used to extract the \BsToKMuNu yield. However a two stage fit is performed in order to first extract the \BsToKMuNu yield in data, and then determine the relative fractions in the high and low \qsq bins. 

The corrected mass distribution for combinatoric \Km\mup combinations in the region below $m_{\Bs}$ is incredibly similar to the \BsToKMuNu corrected mass shape. Both have disappearing tails at low corrected mass, however the \BsToKMuNu peaks at {$m_{\mathrm{corr}}=m_\Bs$} while the combinatoric sample continues to rise. See Figures~\ref{fig:Combi_Dist} and \ref{fig:MCorrBias }. Removing events with no \qsq solution has the unfortunate effect of removing all events with {$m_{\mathrm{corr}}>m_\Bs$}, thus producing almost identical fit distributions. To solve this the two stage fit is used and the results of the first fit are used to constrain yields in the second fit. 
The first fit is performed over the full corrected mass range with no selection on the \qsq allowing the signal and combinatoric distributions to be clearly distinguished in the high corrected mass region, see Figure~\ref{fig:SigFit_NoQ2}. The second fit is a simultaneous fit in high and low bins of \qsq, with all unphysical solutions removed, and uses the results of the first fit to constrain the different yields, see Figure ~\ref{fig:SigFit_Bins}. To summarise, the first fit determines the absolute yields and the second fit determines their fractions in the high and low bins of \qsq. Performing a simultaneous fit in both bins of \qsq without initially Constraining the total yields results in significantly larger uncertainties due to similarity in fit shapes of the signal and combinatoric shapes.

The uncertainties from the first fit are propagated through to the second fit as Gaussian constraints on the combined yield in the high and low \qsq bins. Take for example \BsToKMuNu, in the second fit a Gaussian constraint is applied to the combined yield in both bins with a value equal to the yield from the first fit multiplied by the efficiency of requiring a valid \qsq solution and a width equal to the uncertainty from the first fit. All parameters in the fit are listed in Table~\ref{Tab:SigFitComponents}. As detailed in previous sections the {\Bu\to\jpsi\Kp} yield and yields of misidentified particles are obtained externally to the fit and the yields of the components are given a Gaussian constraint. Additional constraints are used to constrain some yields relative to others, most notably the {\Bs\to\jpsi\phiz} yield is constrained to the {\Bu\to\jpsi\Kp} yield using the knowledge of relative fragmentation fractions, branching fractions and efficiencies. All constraints used in the fit are listed in Table~\ref{Tab:FitConstrs}.




\begin{table}
\centering\renewcommand{\arraystretch}{1.5} % Default value: 1

	\begin{tabular}{l|c|cc}

					  & Fit \#1                           & \multicolumn{2}{c}{Fit \#2 } \\
	                  &\multicolumn{1}{l|}{No \qsq sel.}  &  \multicolumn{1}{c}{$\qsq<7 \gev^2/c^4$}   &  \multicolumn{1}{c}{$\qsq > 7 \gev^2/c^4$} \\\hline

	\BsToKMuNu        &\cellcolor[HTML]{F8F800}$\mathrm{Y}_{\Km\mup}$         & \multicolumn{2}{c}{\cellcolor[HTML]{38FFF8}$\mathrm{Y}_{\Km\mup} \times \varepsilon_{\qsq>0}$}             \\
	\rowcolor[HTML]{F8F802}\cellcolor[HTML]{FFFFFF}
	                  &                                   & $f_{\qsq<7}$          & $1-f_{\qsq<7}$  \\\hline

	\Bs\to\Kstarm\mup\neum  &\cellcolor[HTML]{F8F800}$\mathrm{Y}_{\Kstarm\mup}$& \multicolumn{2}{c}{\cellcolor[HTML]{38FFF8}$\mathrm{Y}_{\Kstarm\mup} \times \varepsilon_{\qsq>0}$} \\
	\rowcolor[HTML]{F8F800}\cellcolor[HTML]{FFFFFF}
	                  &\cellcolor[HTML]{F8F800}                                   & $f_{\qsq<7}$          & $1-f_{\qsq<7}$  \\\hline

	\rowcolor[HTML]{34FF34}\cellcolor[HTML]{FFFFFF}
	\Bu\to\jpsi\Kp    &$\mathrm{Y}_{\jpsi\Kp}$        &$\mathrm{Y}_{\jpsi\Kp~|\qsq<7}$ & $\mathrm{Y}_{\jpsi\Kp~|\qsq>7}$ \\\hline

	\rowcolor[HTML]{34FF34}\cellcolor[HTML]{FFFFFF}
	\Bs\to\jpsi\phiz  &$R\times\mathrm{Y}_{\jpsi\Kp}$           & $R\times\mathrm{Y}_{\jpsi\Kp~|\qsq<7}$ & $R\times\mathrm{Y}_{\jpsi\Kp~|\qsq>7}$ \\\hline

	\bquark\to\cquark\to\squark&\cellcolor[HTML]{F8F800}$\mathrm{Y_{inc.}}$   & \multicolumn{2}{c}{\cellcolor[HTML]{38FFF8}$\mathrm{Y_{inc.}} \times \varepsilon_{\qsq>0}$} \\

	\rowcolor[HTML]{F8F802}\cellcolor[HTML]{FFFFFF}
	                  &                                   & $f_{\qsq<7}$          & $1-f_{\qsq<7}$  \\\hline


	Combinatorics        &\cellcolor[HTML]{F8F800}$\mathrm{Y_{Combi.}}$ &  \cellcolor[HTML]{38FFF8}$\mathrm{Y_{Combi.}} \times \varepsilon_{\qsq<7}$ & \cellcolor[HTML]{38FFF8}$\mathrm{Y_{Combi.}} \times \varepsilon_{\qsq>7}$ \\\hline

	\rowcolor[HTML]{34FF34}\cellcolor[HTML]{FFFFFF}
	$\pi\to K$ MisID     &$\mathrm{Y}_{\pi\to K}$        & $\mathrm{Y}_{\pi\to K~|\qsq<7}$          & $\mathrm{Y}_{\pi\to K~|\qsq>7}$ \\
	\rowcolor[HTML]{34FF34}\cellcolor[HTML]{FFFFFF}
	$\proton\to K$ MisID &$\mathrm{Y}_{\proton\to K}$    & $\mathrm{Y}_{\proton\to K~|\qsq<7}$      & $\mathrm{Y}_{\proton\to K~|\qsq>7}$ \\
	\rowcolor[HTML]{34FF34}\cellcolor[HTML]{FFFFFF}
	$\mu\to K$ MisID     &$\mathrm{Y}_{\mu\to K}$        & $\mathrm{Y}_{\mu\to K~|\qsq<7}$          & $\mathrm{Y}_{\mu\to K~|\qsq>7}$ \\

	\end{tabular}
	\caption[Summary of fit component relationships.]{Components of the two fits used to determine the \BsToKMuNu yields, Y,  in data are presented. Yields shaded in yellow are determined from the fit and left completely free, yields shaded in green are determined externally to the fit and their values are Gaussian constrained, and yields shaded in blue for Fit \#2 are Gaussian constrained to the results obtained from the fit \#1. The {\Bs\to\jpsi\phiz} yield is determined by scaling the {\Bu\to\jpsi\Kp} yield by the relative yields, {$R= f_s/f_d \times  \varepsilon_{\jpsi\phiz}/\varepsilon_{\jpsi\Kp}\times \BF(\jpsi\Kp)/\BF({\jpsi\phiz})$}. All efficiencies, $\varepsilon$, are determined from corrected Monte Carlo simulations. Considering the \BsToKMuNu component, fit \#1 determines the yield and fit \#2 determines the distribution of the yield in the high and low \qsq bins.}
	\label{Tab:SigFitComponents}
\end{table}


\begin{table}[hbt]
\centering
\begin{tabular}{l|ccc}
								& \multicolumn{3}{c}{Physics constraints}\\\hline 

	$f_s/f_d$                   & \multicolumn{3}{c}{ $0.252\pm0.012$~\cite{LHCb:2013lka,Aad:2015cda} }                          \\
	$\BF({\Bu\to\jpsi\Kp})$      & \multicolumn{3}{c}{ $(1.01\pm0.03)\times10^{-3}$~\cite{PDG:2016xqp} }             \\
	$\BF({\Bs\to\jpsi\phiz})$    & \multicolumn{3}{c}{ $(1.08\pm0.08)\times10^{-3}$~\cite{PDG:2016xqp} }             \\
	$\BF({\phiz\to\Km\Kp})$      & \multicolumn{3}{c}{ $0.492\pm0.005$~\cite{PDG:2016xqp}}                           \\\hline

								& \multicolumn{3}{c}{Yield constraints} \\
	                            & No \qsq sel.   & $\qsq < 7\gev^2/c^4$  & $\qsq > 7\gev^2/c^4$  \\\hline

	$\Bu\to\jpsi\Kp$            & $2357\pm 127$  & $279\pm43$            & $1607\pm154$          \\
	$\Bs\to\jpsi\phiz$          & $62\pm3$       & $6\pm 3$              & $47\pm5$              \\
	$\pi\to K$ MisId            & $762\pm23$     & $496\pm18$            & $243\pm13$            \\
	$\proton\to K$ MisId        & $911\pm 47$    & $320\pm28$            & $512\pm35$            \\
	$\mu\to K$ MisId            & $121\pm 5$     & $86\pm4$              & $33\pm 3$             \\\hline

								&          \multicolumn{3}{c}{Simultaneous Fit yield constraints}\\\hline
	\BsToKMuNu                  &                & \multicolumn{2}{c}{$8550 \pm760$}             \\
	$\Bs\to\Kstarm\mup\neum$    &                & \multicolumn{2}{c}{$1760 \pm350$}             \\
	$\bquark\to\cquark\to\squark$&               & \multicolumn{2}{c}{$35580\pm740$}             \\
	Combinatorics               &                & $790\pm160$           & $622\pm120$            \\
\end{tabular}
	\caption[Summary of constraints for the \BsToKMuNu signal fit]{A summary of the constraints and fit values entering the signal fit. During the second fit some values are constrained in both the high and low \qsq bins, e.g. the combinatoric yield, while for other components the combined sum of entries in both the high and low \qsq bins is constrained, e.g. the \BsToKMuNu yield.}
	\label{Tab:FitConstrs}
\end{table}



\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.7\textwidth]{Fit/No_Q2FitPlot_Minimal3.pdf}
	\caption[\BsToKMuNu signal fit to the \Km\mup corrected mass.]{A fit to the corrected \Km\mup mass distribution for data candidates passing the selections. The uncertainty in the predicted data yield for each bin is shaded in grey. The pulls for each bin, $i$, are shown underneath the fit.}
	\label{fig:SigFit_NoQ2}
\end{figure}


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.495\textwidth]{Fit/Low_Q2_FitPlot_Minimal3.pdf}
	\includegraphics[width=0.495\textwidth]{Fit/High_Q2_FitPlot_Minimal3.pdf}
	\caption[\BsToKMuNu signal fit to the \Km\mup corrected mass performed simultaneously in two \qsq bins.]{A simultaneous fit in two bins of \qsq performed on the corrected \Km\mup mass distribution for data candidates passing the selections. }
	\label{fig:SigFit_Bins}
\end{figure}



\subsection{Fit Results}
\label{ssec:SF_Results}

Results from the first signal fit to determine the \BsToKMuNu yield using the corrected \Km\mup mass are plotted in Figure~\ref{fig:SigFit_NoQ2}. The observed number of \BsToKMuNu events is $10050\pm880$. A significant peaking structure is observed in the corrected \Km\mup mass distribution, at the mass of the \Bs meson. This corresponds to the decay \BsToKMuNu and this peaking structure is the discovery of \BsToKMuNu decays. The family of decays {$\Bs\to\Kstarm\mup\neum$} is also observed for the first time although the individual contributions from the $\Kstarm(892)$, $K^{*-}_2(1430)$ and $K^{*-}_0(1430)$ are not individually measured.
The results of the second fit are plotted in Figure~\ref{fig:SigFit_Bins}, for the low (left) and high (right) \qsq bins, the signal purity is considerably higher in the low \qsq bin and the \BsToKMuNu contribution is clearly required in order to account for the large number of events in the high corrected mass region. The signal yield in the low \qsq bin is $5160\pm470$, and in the high \qsq bin is $3280\pm430$. 




A Monte Carlo method is used to validate the signal fit results, 1000 or 500 pseudo-datasets are generated by randomly sampling the Monte Carlo input input histograms. The \BsToKMuNu yield in the pseudo-dataset is constant and set to the value obtained in the fits, the yields of all other components are randomly varied by selecting a point on a Gaussian distribution with a mean centred on the fit result with a width set to the fit uncertainty. The distribution of toy pulls should follow the normal distribution, be centred at zero and have a width of one. An offset distribution is indicative of biases present in the fit and a width differing from one indicates that the uncertainty on the fitted yield is being incorrectly estimated.


The pull distributions of the first fit are plotted in Figure~\ref{fig:KMuNu_Pulls_No}, a slight offset of $0.14\sigma$ is observed and the width is slightly less than one indicating that the uncertainty on the signal yield is being overestimated. The pulls for second fit are plotted in Figure~\ref{fig:KMuNu_Pulls_Bins_Low_High} for both the \BsToKMuNu and {inclusive $\bquark\to\cquark\to\squark$} contributions, an offset of $0.50\sigma$ and $0.62\sigma$ is observed in the low and high \qsq bins respectively indicating significant biasing. The widths are 0.85 and 1.01 indicating that the fit uncertainty is being overestimated in the low \qsq bin. The inclusive {$\bquark\to\cquark\to\squark$} sample shows an offset of $-0.37\sigma$ and $-0.40\sigma$ in the high and low bins respectively indicating that the fit is unable to fully distinguish the two samples. Systematic uncertainties are assigned from the biases observed here.

\begin{figure}[H]
	\centering
	%\includegraphics[width=0.495\textwidth, page=1]{Fit/ToyPlots_K.pdf}
	\includegraphics[width=0.495\textwidth, page=2]{Fit/ToyPlots_K.pdf}
	\caption[\BsToKMuNu signal fit pull distributions.]{Distributions of pulls  obtained from 1000 fits to pseudo-datasets. The pull is defined as the difference between the true number of \BsToKMuNu candidates and the yield obtained from the fit divided by the fit uncertainty.}
	\label{fig:KMuNu_Pulls_No}

	\includegraphics[width=0.495\textwidth, page=3]{Fit/ToyPlots_K_2D.pdf}
	\includegraphics[width=0.495\textwidth, page=4]{Fit/ToyPlots_K_2D.pdf}
	\caption[\BsToKMuNu signal fit pull distributions from a simultaneous fit in two \qsq bins.]{Distributions of pulls obtained from 500 fits to pseudo-datasets for the low (left) and high (right) \qsq bins.}
	\label{fig:KMuNu_Pulls_Bins_Low_High}

	\includegraphics[width=0.495\textwidth, page=11]{Fit/ToyPlots_K_2D.pdf}
	\includegraphics[width=0.495\textwidth, page=12]{Fit/ToyPlots_K_2D.pdf}
	\caption[\bquark\to\cquark\to\squark signal fit pull distributions from a simultaneous fit in two \qsq bins.]{Distributions of pulls for the {\bquark\to\cquark\to\squark} template obtained from 500 fits to pseudo-datasets for the low (left) and high (right) \qsq bins.}
	\label{fig:Incl_Pulls_Bins_Low_High}

\end{figure}


\section{Systematic Uncertainties}
\label{sec:SF_Systematics}

The uncertainty on the \BsToKMuNu yield obtained from the signal fit contains several systematic uncertainties. The fit does not account for systematics originating from the variation of the corrected mass shape associated with varying form factor models, the uncertainty of the {$\Bu\to\jpsi\Kp$} yield due to a limited knowledge of the reconstruction efficiency. Additionally the uncertainty on the signal and background yields does not consider the fact that the signal fit may be biased. For the fit used to obtain the \BsToDsMuNu yield the only systematic effect considered is the bias present in the fit. 

The systematic uncertainty originating from the variation in the corrected mass template shape is investigated by generating the template shape with different corrections and weights applied. The \BsToKMuNu template shape is plotted in Figure~\ref{fig:MCorrBias } reconstructed using all form factor hypotheses, with and without the addition of weights correcting Monte Carlo Simulation. A systematic uncertainty originating from the uncertainty in the corrected mass template shape is determined by repeating the signal using different possible template shapes. The systematic uncertainty assigned to variations in the template shape are summarised in Table~\ref{Tab:FitTemplateSyst}. As discussed in Section~\ref{ssec:FF_Calc} the uncertainty on the form factor shape is lowest at high \qsq resulting in a greater variation in the corrected mass template in the low \qsq bin; this is reflected in the calculated systematic uncertainty.


\begin{figure}[ht]
	\centering
	\includegraphics[width=0.6\textwidth]{root/Bs_MCORR_Bias.pdf}
	\caption[Impact of reweighting on corrected mass distribution]{The Corrected \Km\mup mass distribution for simulated \BsToKMuNu decays is plotted with different corrections applied. The shaded grey region consists of the uncorrected Monte Carlo with full selections applied. The lines display Monte Carlo with form factor corrections applied and the points represent Monte Carlo with form factor and kinematic corrections applied. }
	\label{fig:MCorrBias }
\end{figure}

A systematic uncertainty on the${\Bu\to\jpsi\Kp}$yield due to an uncertainty on the parametrisation of the background shape is detailed in Section~\ref{ssec:Bu_JPsiKConstr} and the systematic uncertainty on the \BsToKMuNu yield is given in Table~\ref{Tab:FitTemplateSyst}.

The systematic uncertainty on the \BsToKMuNu and \BsToDsMuNu yield associated with a biased fit is quantified in Sections~\ref{ssec:SF_Results}~and~\ref{ssec:NF_Results} by performing 1000 or 500 fits to pseudo-data. The systematic uncertainty is assigned by taking the mean of the pull distributions on the \BsToKMuNu and \BsToDsMuNu yields of 1000 or 500 fits to pseudodata, and are listed in Table~\ref{Tab:FitTemplateSyst}.




\begin{table}[h!]
	\centering
	\begin{tabular}{r|lll}
		$\sigma_{\mathrm{syst.}} [\%]$  &No \qsq Sel.         & $\qsq < 7\gev^2/c^4$ & $\qsq > 7\gev^2/c^4$ \\\hline
		Template variation              & 1.36                & 3.64                 & 0.87 \\
		\Bu\to\jpsi\Kp reconstruction   & 2.07                & 0.61                 & 3.79 \\
		Fit Bias, \BsToKMuNu            & 1.22                & 4.55                 & 8.09 \\
		Fit Bias, \BsToDsMuNu           & 0.59                &                      &      \\
	\end{tabular}
	\caption[\BsToKMuNu fit yield systematics]{Systematic uncertainties on the \BsToKMuNu yield due uncertainties on the template shape, {\Bu\to\jpsi\Kp} reconstruction and biases within the fit. The systematic uncertainty on the \BsToDsMuNu yield due to biases in the fit is included.}
	\label{Tab:FitTemplateSyst}

\end{table}

