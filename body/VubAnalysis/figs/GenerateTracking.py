from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
import ROOT.RooFit as RF

gROOT.ProcessLine(".x ../../../lhcbStyle.C")

ROOT.gStyle.SetPalette(ROOT.kDarkBodyRadiator)

f = ROOT.TFile.Open("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Histogrammer/Input/Tracking/ratio2012S20_MeV.root", "READ")


h = f.Get("Ratio")
h.SetTitle("")
h.GetZaxis().SetRangeUser(0.8, 1.2)
h.SetMarkerSize(h.GetMarkerSize()*3)

c = ROOT.TCanvas("c", "", 1600, 1200)
c.SetLogx()
h.Draw("COL")


txts = []
for xbin in range(1, h.GetNbinsX()+1):
	for ybin in range(1, h.GetNbinsY()+1):
		
		xlow  = h.GetXaxis().GetBinLowEdge(xbin)
		ylow  = h.GetYaxis().GetBinLowEdge(ybin)
		xhigh = h.GetXaxis().GetBinLowEdge(xbin+1)
		yhigh = h.GetYaxis().GetBinLowEdge(ybin+1)
		
		txt = ROOT.TPaveText(xlow, ylow, xhigh, yhigh)
		txt.AddText(0, 0.55, "{0:.3f}".format(h.GetBinContent(xbin, ybin)))
		txt.AddText(0, 0.4, "({0:.3f})".format(h.GetBinError(xbin, ybin)))
		txt.SetTextSize(0.055)
		txt.SetFillColorAlpha(0,0)
		txt.SetBorderSize(1)
		txt.Draw()

		txts += [txt]
c.Print("TrackingTable.pdf")

