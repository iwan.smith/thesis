import ROOT
import numpy as np

ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
ROOT.gStyle.SetOptStat(0)


c = ROOT.TCanvas("c", "c", 1600, 1200)


Vcb_Inc = (0.0422e3, 0.0008e3) 
Vcb_Exc = (0.0412e3, 0.0011e3)


Vub_Inc = (0.00444e3, 0.00035e3)
Vub_Exc = (0.00372e3, 0.00024e3)

VubVcb_Lb     = (0.080,   0.0056)
VubVcb_Bs     = (0.06879, 0.01035)
VubVcb_Bs_Low = (0.06254, 0.01002)


h_Vub_Low  = 1.4
h_Vub_High = 6.6
h_Vcb_Low  = 35.
h_Vcb_High = 48.



Box = ROOT.TH2F("h", ";|V_{cb}| #times 10^{3};|V_{ub}| #times 10^{3}", 100, h_Vcb_Low, h_Vcb_High, 100, h_Vub_Low, h_Vub_High)
Box.GetYaxis().SetTitleOffset(Box.GetYaxis().GetTitleOffset()*0.8)

B_Vcb_Inc = ROOT.TBox(Vcb_Inc[0]-Vcb_Inc[1], h_Vub_Low, Vcb_Inc[0]+Vcb_Inc[1],h_Vub_High)
B_Vcb_Exc = ROOT.TBox(Vcb_Exc[0]-Vcb_Exc[1], h_Vub_Low, Vcb_Exc[0]+Vcb_Exc[1],h_Vub_High)

B_Vub_Inc = ROOT.TBox(h_Vcb_Low, Vub_Inc[0]-Vub_Inc[1], h_Vcb_High, Vub_Inc[0]+Vub_Inc[1])
B_Vub_Exc = ROOT.TBox(h_Vcb_Low, Vub_Exc[0]-Vub_Exc[1], h_Vcb_High, Vub_Exc[0]+Vub_Exc[1])

Box.Draw()
B_Vcb_Inc.Draw("fl")
B_Vcb_Exc.Draw("fl")
B_Vub_Inc.Draw("fl")
B_Vub_Exc.Draw("fl")

B_Vcb_Inc.SetFillColorAlpha(2, 0.2)
B_Vcb_Exc.SetFillColorAlpha(3, 0.2)
B_Vub_Inc.SetFillColorAlpha(2, 0.2)
B_Vub_Exc.SetFillColorAlpha(4, 0.2)

B_Vcb_Inc.SetLineWidth(2)
B_Vcb_Exc.SetLineWidth(2)
B_Vub_Inc.SetLineWidth(2)
B_Vub_Exc.SetLineWidth(2)


B_Vcb_Inc.SetLineColorAlpha(2, 0.5)
B_Vcb_Exc.SetLineColorAlpha(3, 0.5)
B_Vub_Inc.SetLineColorAlpha(2, 0.5)
B_Vub_Exc.SetLineColorAlpha(4, 0.5)


VubVcb_Lb_x = np.asarray([ h_Vcb_Low ,                             h_Vcb_Low,                             h_Vcb_High,                             h_Vcb_High,                             h_Vcb_Low                             ], dtype=np.float32)
VubVcb_Lb_y = np.asarray([ h_Vcb_Low*(VubVcb_Lb[0]-VubVcb_Lb[1]) , h_Vcb_Low*(VubVcb_Lb[0]+VubVcb_Lb[1]), h_Vcb_High*(VubVcb_Lb[0]+VubVcb_Lb[1]), h_Vcb_High*(VubVcb_Lb[0]-VubVcb_Lb[1]), h_Vcb_Low*(VubVcb_Lb[0]-VubVcb_Lb[1]) ], dtype=np.float32)

B_VubVcb_Lb = ROOT.TPolyLine(5, VubVcb_Lb_x, VubVcb_Lb_y)
B_VubVcb_Lb.SetFillColorAlpha(6, 0.2)
B_VubVcb_Lb.SetLineColorAlpha(6, 0.5)
B_VubVcb_Lb.SetLineWidth(2)
B_VubVcb_Lb.Draw("f")
B_VubVcb_Lb.Draw()

VubVcb_Bs_x = np.asarray([ h_Vcb_Low ,                             h_Vcb_Low,                             h_Vcb_High,                             h_Vcb_High,                             h_Vcb_Low                             ], dtype=np.float32)
VubVcb_Bs_y = np.asarray([ h_Vcb_Low*(VubVcb_Bs[0]-VubVcb_Bs[1]) , h_Vcb_Low*(VubVcb_Bs[0]+VubVcb_Bs[1]), h_Vcb_High*(VubVcb_Bs[0]+VubVcb_Bs[1]), h_Vcb_High*(VubVcb_Bs[0]-VubVcb_Bs[1]), h_Vcb_Low*(VubVcb_Bs[0]-VubVcb_Bs[1]) ], dtype=np.float32)

B_VubVcb_Bs = ROOT.TPolyLine(5, VubVcb_Bs_x, VubVcb_Bs_y)
B_VubVcb_Bs.SetFillColorAlpha(1, 0.2)
B_VubVcb_Bs.SetLineColorAlpha(1, 1.0)
B_VubVcb_Bs.SetLineWidth(2)
B_VubVcb_Bs.Draw("f")
B_VubVcb_Bs.Draw()

VubVcb_Bs_Low_x = np.asarray([ h_Vcb_Low ,                                     h_Vcb_Low,                                     h_Vcb_High,                                     h_Vcb_High,                                     h_Vcb_Low                                     ], dtype=np.float32)
VubVcb_Bs_Low_y = np.asarray([ h_Vcb_Low*(VubVcb_Bs_Low[0]-VubVcb_Bs_Low[1]) , h_Vcb_Low*(VubVcb_Bs_Low[0]+VubVcb_Bs_Low[1]), h_Vcb_High*(VubVcb_Bs_Low[0]+VubVcb_Bs_Low[1]), h_Vcb_High*(VubVcb_Bs_Low[0]-VubVcb_Bs_Low[1]), h_Vcb_Low*(VubVcb_Bs_Low[0]-VubVcb_Bs_Low[1]) ], dtype=np.float32)

B_VubVcb_Bs_Low = ROOT.TPolyLine(5, VubVcb_Bs_Low_x, VubVcb_Bs_Low_y)
B_VubVcb_Bs_Low.SetFillColorAlpha(0, 0.)
B_VubVcb_Bs_Low.SetLineColorAlpha(1, 1.)
B_VubVcb_Bs_Low.SetLineStyle(2)
B_VubVcb_Bs_Low.SetLineWidth(2)
B_VubVcb_Bs_Low.Draw("f")
B_VubVcb_Bs_Low.Draw()

T_Vcb_Inc = ROOT.TLatex(Vcb_Inc[0]+0.9*Vcb_Inc[1], h_Vub_High-0.15, u"|V_{cb}| Inclusive")
T_Vcb_Inc.SetTextAngle(90)
T_Vcb_Inc.SetTextAlign(30)
T_Vcb_Inc.SetTextSize(0.04)

T_Vcb_Exc = ROOT.TLatex(Vcb_Exc[0]-0.5*Vcb_Exc[1], h_Vub_High-0.15, u"|V_{cb}| Exclusive")
T_Vcb_Exc.SetTextAngle(90)
T_Vcb_Exc.SetTextAlign(30)
T_Vcb_Exc.SetTextSize(0.04)

T_Vub_Inc = ROOT.TLatex(h_Vcb_Low+0.4, Vub_Inc[0], u"|V_{ub}| Inclusive")
T_Vub_Inc.SetTextAlign(12)
T_Vub_Inc.SetTextSize(0.04)

T_Vub_Exc = ROOT.TLatex(h_Vcb_Low+0.4, Vub_Exc[0], u"|V_{ub}| Exclusive")
T_Vub_Exc.SetTextAlign(12)
T_Vub_Exc.SetTextSize(0.04)

T_VubVcb_Lb = ROOT.TLatex(h_Vcb_Low+0.4, (h_Vcb_Low+0.4)*(VubVcb_Lb[0]+1.5*VubVcb_Lb[1]), u"#Lambda_{b} #rightarrow p #mu^{-} #bar{#nu_{#mu}}")
T_VubVcb_Lb.SetTextAngle(7.5)
T_VubVcb_Lb.SetTextSize(0.04)

T_VubVcb_Bs = ROOT.TLatex(h_Vcb_Low+0.4, (h_Vcb_Low+0.4)*(VubVcb_Bs[0]-0.8*VubVcb_Bs[1]), u"B_{s}^{0} #rightarrow K^{-} #mu^{+} #nu_{#mu}")
T_VubVcb_Bs.SetTextAngle(7.5)
T_VubVcb_Bs.SetTextSize(0.04)

T_Vcb_Inc.Draw()
T_Vcb_Exc.Draw()
T_Vub_Inc.Draw()
T_Vub_Exc.Draw()
T_VubVcb_Lb.Draw()
T_VubVcb_Bs.Draw()
c.Print("VubFinalResult_CKMFit.pdf")
