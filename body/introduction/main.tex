\input{body/introduction/title}

The standard model of particle physics is the name given in the 1970s to the theory describing all fundamental particles and the forces governing their interactions. It incorporates all that we know about subatomic particles and has predicted the existence of new particles, most famously the Higgs boson which was discovered in 2012 by the \atlas and \cms experiments.
The 17 particles in the standard model are divided into six quarks, six leptons, four gauge bosons and one scalar boson. The six quarks can be divided into three up-down pairs and the six leptons can be divided into three pairs containing a charged lepton and a neutrino. Quark and lepton pairs are known as flavours. Different quark and lepton pairs behave in exactly the same way and the masses of the charged leptons and quarks originate from their coupling to the Higgs field, with masses varying by five orders of magnitude in the quark sector and three orders of magnitude in the charged lepton sector. It remains unknown why the masses vary to such an extent and why there are exactly three flavours of quarks and leptons.


The standard model allows quarks to change flavour via the charged weak interaction mediated by the \Wpm boson, a process that was first observed in 1896 via the radioactive decay of a neutron to proton via the emission of a \Wpm.
\begin{equation}
\neutron\to\proton\en\neueb,
\end{equation}
in which a neutron, $\uquark\uquark\dquark$, decays to a proton, $\uquark\dquark\dquark$, The weak force only couples leptons of the same generation and for quarks cross-generational couplings are allowed. The strength of the couplings between quarks are proportional to the elements of the Cabibbo-Kobayashi-Maskawa (CKM) matrix
\begin{equation}
	V^{CKM} = 
	\left(\begin{matrix}
		\Vud & \Vus & \Vub \\
		\Vcd & \Vcs & \Vcb \\
		\Vtd & \Vts & \Vtb
	\end{matrix}\right),
\end{equation}
and the couplings between charged leptons and neutrinos are universal across the generations. The structure of the CKM matrix is nearly diagonal and is illustrated in Figure~\ref{fig:CKM_Vis}, with \AbsVub being the smallest and least well known of the elements. It is worth noting that the CKM matrix is unitary,
\begin{equation}
	\sum_i^3 V_{ij}V_{ik}^* = \delta_{jk}
	\label{eq:CKMUnitary}
\end{equation}
 which provides an essential test of the Standard Model. The vanishing combinations of Equation~\ref{eq:CKMUnitary} can be represented as triangles in the complex plane known as CKM unitary triangles.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.45\textwidth]{CKMMatrix.pdf} \hfill
	\includegraphics[width=0.45\textwidth]{CKMFracError_NoLog.pdf}
	\caption[Visualisation of CKM matrix.]{The magnitudes of the CKM matrix elements are illustrated (left) with the almost diagonal structure clearly visible. The fractional uncertainties of the CKM matrix elements are plotted on the right and it can be seen that \AbsVub is the smallest element with the largest relative uncertainty.}
	\label{fig:CKM_Vis}
\end{figure}

The CKM matrix is parametrised by three mixing angles and a complex phase. The complex phase is responsible for all CP violation in the standard model. The CP operator is a product of the charge conjugation operator, $C\en\to\ep$, and the parity transformation operator, $Px_i \to -x_i$. CP violation is responsible for the difference in behaviour between matter and antimatter and is required to explain the matter-antimatter asymmetry we observe in the universe. However the amount of CP violation required to explain the matter-antimatter asymmetry we see today is nine orders of magnitude larger than seen in the quark sector. 


In order to test the unitarity of the CKM matrix and precisely measure the amount of CP violation in the quark sector, the parameters of the CKM matrix must be constrained. The CKM parameters can be constrained by performing measurements of observables sensitive to the magnitudes of the CKM matrix elements. Since \AbsVub is the least well known of the CKM matrix elements it is the dominant limiting factor when drawing CKM unitary triangles. An improved uncertainty on \AbsVub will improve the global precision of fits to the CKM unitary triangles and test the unitarity of the CKM matrix. Non unitarity of the CKM matrix would be indicative of new physics beyond the standard model.



The CKM matrix elements \AbsVub and \AbsVcb can be determined from inclusive and exclusive semileptonic decays of a \B hadron. When performing an exclusive measurement all visible\footnote{A visible particle is reconstructible by the detector. The neutrino is considered invisible.} decay products of the \B are reconstructed, and an inclusive decay, $\B\to \ellm\neub X$, contains additional unreconstructed final state particles. 
Inclusive determinations of \AbsVcb combine measurements of the semileptonic $\bquark\to\cquark\mun\neumb X$  decay rate with the leptonic energy, the hadronic invariant mass spectra and theoretical calculations. Inclusive measurements of \AbsVcb were initially performed by the ARGUS and CLEO collaborations. Later came the \B factories operating at the \FourS production energy and \lep using \B mesons produced from the decays of the \Z boson. The \B factories had the benefit of higher statistics and produced more precise determinations than \lep while the boosted \B mesons from the \Z allowed measurements to be made in a larger phase space.

An inclusive measurement of \AbsVub is complicated due to the enormous backgrounds originating from $\B\to X_c \ellm\neub$ decays. A kinematic approach is usually taken and inclusive measurements are performed in the region where charm backgrounds are kinematically forbidden although statistics can be increased by extending the phase space into the $\B\to X_c \en\neub$ region. \cleo, \belle and \babar have quoted partial rates of $\B\to X_c \ellm\neub$ for $|\vec{p_e}| \geq 2.0\gev$ and $|\vec{p_e}| \geq 1.9\gev$ which is well below the charm kinematic endpoint.

Exclusive determinations of \AbsVcb are based on semileptonic $\B\to\D^{(*)}\ellm\neub$ decays in the limit $m_{b,c,} \gg \lqcd$. Exclusive measurements of \AbsVub are made by combining the exclusive decay rate of \B hadrons combined with form factor predictions. The decays \BToPiMuNu, \BsToKMuNu and \LbToPMuNu which contain a ground state hadron in the final state are ``golden modes'' for lattice QCD predictions and have the lowest theoretical uncertainties.

Form factors provided by lattice QCD are most accurate in the kinematic region with high momentum transfer.

The averaged \AbsVcb measurements are
\begin{equation}
	\AbsVcb_{Incl} = (42.2\pm0.8)\times10^{-3},\hspace*{10mm} 
	\AbsVcb_{Excl} = (41.9\pm2.0)\times10^{-3},
\end{equation}
and the averaged \AbsVub measurements are
\begin{equation}
	\AbsVub_{Incl} = (4.49\pm0.28)\times10^{-3},\hspace*{10mm} 
	\AbsVub_{Excl} = (3.70\pm0.16)\times10^{-3}.
\end{equation}

The difference between inclusive and exclusive measurements of \AbsVcb and \AbsVub of approximately $3\sigma$ has been a long-standing puzzle in particle physics.

The \lhc experiment provides an abundance of \B hadrons which are detected by the \lhcb experiment making exclusive determinations of \AbsVub possible with the decays \LbToPMuNu and \BsToKMuNu. This thesis presents a first observation of the decay \BsToKMuNu with a measurement of the ratio of branching fractions $\frac{\BF(\BsToKMuNu)}{\BF(\BsToDsMuNu)}$ and a ratio of the CKM matrix elements $\AbsVub/\AbsVcb$. This measurement uses data collected from \pp collision events collected by the \lhcb experiment in the year 2012. 
The measured ratio of branching fractions is combined with  theoretical inputs from Lattice QCD and Light-Cone Sum Rules allowing $\AbsVub/\AbsVcb$ to be determined. This ratio provides an important constraint when performing global fits testing the unitarity of the CKM matrix.

The thesis is structured as follows. Chapter~2 presents an overview of the theoretical framework required for this measurement, including a discussion of the standard model of particle physics and the CKM sector. The theory of semileptonic decays is presented alongside the theory of lattice QCD and the latest form factor predictions for the decays \BsToKMuNu and \BsToDsMuNu are presented.
The LHC and \lhcb experiments are introduced in Chapter~3 and the conditions for taking data are discussed. Chapter~4 briefly discusses the analysis strategy for the measurement of the ratio of branching fractions and CKM matrix elements. The main analysis work is presented in Chapters~5 and~6 and discusses the methods used to reconstruct \BsToKMuNu candidates and separate a signal yield from the many backgrounds present at the \lhcb experiment. Chapter 5 details the reconstruction of several non-trivial kinematic distributions essential for this analysis and goes on to detail the modelling of data and the selections used to reject backgrounds. Chapter~6 goes on to detail the fits used to extract the \BsToKMuNu and \BsToDsMuNu yields in data followed by a calculation of the selection efficiencies and systematics, and culminates with the results of ${\BF(\BsToKMuNu)/\BF(\BsToDsMuNu)}$ and $\AbsVub/\AbsVcb$.
The implications of this measurement on the particle physics landscape is discussed in Chapter~7 which leads to the conclusion of this thesis in Chapter 8.



\begin{comment}
\begin{equation}
V^{CKM} = 
\left(\begin{matrix}
	\Vud & \Vus & \Vub \\
	\Vcd & \Vcs & \Vcb \\
	\Vtd & \Vts & \Vtb
\end{matrix}\right)
=
\left(\begin{matrix}
% Numbers taken from PDG review chapter 12 CKM Quark mixing matrix
	0.97417 \pm 0.00021 & 0.2248 \pm 0.0006 & 0.00409 \pm 0.00039 \\
	0.220   \pm 0.005   & 0.995  \pm 0.016  & 0.0405  \pm 0.0015  \\
	0.0082  \pm 0.0006  & 0.0400 \pm 0.0027 & 1.009   \pm 0.031  
\end{matrix}\right)
\end{equation}
\end{comment}
