#include "TH1D.h"
#include "TCanvas.h"
#include "TStyle.h"
#include <cstdlib>

struct CKMElement
{
	std::string Name;
	double      Value;
	double      Error;
	int         Colour;

	TBox*       Box;

};

bool SortbyError( CKMElement& lhs, CKMElement& rhs)
{
	return (lhs.Error/lhs.Value) < (rhs.Error/rhs.Value);
}


void CKMFigs()
{
	gROOT->ProcessLine(".L lhcbStyle.C");
	gStyle->SetOptStat(0);
	
	std::vector<CKMElement> Elements(9);
	Elements[0] = CKMElement{"|V_{tb}|", 1.009,    0.031,      kRed     };
	Elements[1] = CKMElement{"|V_{cs}|", 0.995,    0.016,      kPink+5  };
	Elements[2] = CKMElement{"|V_{ud}|", 0.97417,  0.00021,    kViolet-6};
	Elements[3] = CKMElement{"|V_{us}|", 0.2248,   0.0006,     kBlue+1  };
	Elements[4] = CKMElement{"|V_{cd}|", 0.220,    0.005,      kTeal-1  };
	Elements[5] = CKMElement{"|V_{cb}|", 0.0405,   0.0015,     kGreen+2 };
	Elements[6] = CKMElement{"|V_{ts}|", 0.0400,   0.0027,     kYellow+2};
	Elements[7] = CKMElement{"|V_{td}|", 0.0082,   0.0006,     kOrange+7};
	Elements[8] = CKMElement{"|V_{ub}|", 0.00409,  0.00039,    kBlack   };


	TCanvas* c = new TCanvas("c", "c", 1600, 1600);
	TH1D* h = new TH1D("h", ";;Magnitude", 9, 0, 9);
	
	////////////////////////////////////////
	//
	//  Plot the Magnitude of Vij
	//
	////////////////////////////////////////

	int Bin = 9;
	while ( Bin --> 0)
	{
		h->GetXaxis()->SetBinLabel (Bin+1, ("#color["+ std::to_string(Elements[Bin].Colour) + "]{" + Elements[Bin].Name + "}").c_str());
		h->GetXaxis()->SetLabelSize(0.07);
		//h->GetXaxis()->SetBinLabelColor(Elements[Bin].Colour);
	}	

	h->SetMinimum(0);
	h->SetMaximum(1.2);
	h->Draw();

	Bin=9;
	while ( Bin --> 0)
	{
		TH1D* h2 = (TH1D*)h->Clone();
		h2->SetBinContent(Bin+1, Elements[Bin].Value );
		h2->SetBinError  (Bin+1, Elements[Bin].Error );
		h2->SetLineColor(        Elements[Bin].Colour);
		h2->SetMarkerSize(0);
		h2->SetLineWidth(3);

		h2->Draw("SAME");
		h2->SetMinimum(0.002);
	}
	
	cout << h->GetYaxis()->GetTitleOffset() << endl;
	h->GetYaxis()->SetTitleOffset(1.05);

	cout << c->GetLeftMargin() << endl;
	c->SetLeftMargin(0.17);
	c->SetBottomMargin(0.1);
	
	c->Print("CKMError_NoLog.pdf");
	h->SetMinimum(0.002);
	c->SetLogy();
	c->Print("CKMError_Log.pdf");
	
	////////////////////////////////////////
	//
	//  Plot the Fractional Uncertainty of Vij
	//
	////////////////////////////////////////

	c->SetLogy(false);

	sort(Elements.begin(), Elements.end(), &SortbyError);

	
	h = new TH1D("h3", ";;Fractional Uncertainty", 9, 0, 9);	
	
	Bin = 9;
	while ( Bin --> 0)
	{
		h->GetXaxis()->SetBinLabel (Bin+1, ("#color["+ std::to_string(Elements[Bin].Colour) + "]{" + Elements[Bin].Name + "}").c_str());
		h->GetXaxis()->SetLabelSize(0.07);
		//h->GetXaxis()->SetBinLabelColor(Elements[Bin].Colour);
	}	

	h->SetMinimum(0.00);
	h->SetMaximum(0.105);
	h->Draw();

	Bin=9;
	while ( Bin --> 0)
	{
		TH1D* h2 = (TH1D*)h->Clone();
		h2->SetBinContent(Bin+1, Elements[Bin].Error/Elements[Bin].Value );
		h2->SetFillColor(        Elements[Bin].Colour);
		h2->SetMarkerSize(0);
		h2->SetLineWidth(0);
		h2->SetLineColor(1);
		h2->SetBarWidth(0.5);
		h2->SetBarOffset(0.25);
		h2->Draw("B SAME");
	}
	
	cout << h->GetYaxis()->GetTitleOffset() << endl;
	h->GetYaxis()->SetTitleOffset(1.2);

	cout << c->GetLeftMargin() << endl;
	c->SetLeftMargin(0.17);
	c->SetBottomMargin(0.1);
	
	c->Print("CKMFracError_NoLog.pdf");
	h->SetMinimum(0.002);
	c->SetLogy();
	c->Print("CKMFracError_Log.pdf");
	
	sort(Elements.begin(), Elements.end(), &SortbyError);
	
	
	
	/////////////////////////////////////////////////
	//
	//  Plot the Diagonal Structure of the CKM matrix
	//
	/////////////////////////////////////////////////


	TCanvas* c2 = new TCanvas("c2", "c2", 1600, 1600);


	// Margin of 0.19:
	// centres at 0.325, 0.595, 0.865

	int Box = 9;
	while ( Box --> 0 )
	{
		CKMElement& E = Elements[Box];

		int Row = 0;
		int Col = 0;

		if ( E.Name[4] == 'u')
			Row =1;
		if ( E.Name[4] == 'c')
			Row =2;
		if ( E.Name[4] == 't')
			Row =3;

		if ( E.Name[5] == 'd')
			Col =1;
		if ( E.Name[5] == 's')
			Col =2;
		if ( E.Name[5] == 'b')
			Col =3;

		double c_row = 0.19 + 0.81* (Row-0.5)/3;
		double c_col = 0.19 + 0.81* (Col-0.5)/3;

		double V = E.Value * 0.81 / 3.1;
		TBox* Box = new TBox(c_col-V/2, 1-c_row-V/2 + 0.19, c_col+V/2, 1-c_row+V/2+0.19);
		Box->SetFillColor(E.Colour);

		cout << Row << "  " << Col << "  " << c_col-V/2 << "  " <<  c_row-V/2<< "  " <<  c_col+V/2<< "  " <<  c_row+V/2 << endl;

		if ( Col == 1)
		{
 			TText *t = new TLatex(0.1,1-c_row+0.19, (std::string("#it{") + E.Name[4] + "}").c_str());
 			cout << t ->GetTextSize() << endl;
 			t->SetTextSize(0.1);
 			t->Draw();
		}

		if ( Row == 1)
		{
 			TText *t = new TLatex(c_col,0.1, (std::string("#it{") + E.Name[5] + "}").c_str());
 			t->SetTextSize(0.1);
 			t->Draw();
		}

		Box->Draw();


	}



	c2->Print("CKMMatrix.pdf");
	
	
	
	
	
	
}
