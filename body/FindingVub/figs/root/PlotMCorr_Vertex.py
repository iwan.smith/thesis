
from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
import ROOT.RooFit as RF

gROOT.ProcessLine(".x lhcbStyle.C")

f = ROOT.TFile.Open("FitterOutput.root")


c1 = f.Get("c_Fit_K_0.0")

Colours = [1,2,3,4,6]

i = 0
for h in c1.GetListOfPrimitives():
	
	if not h.InheritsFrom("TH1"):
		continue
	
	if not h.GetName().startswith("Prediction"):
		h.SetLineWidth(0)
		h.SetMarkerSize(0)
		continue
	
	h.SetLineWidth(3)
	h.SetLineColor(Colours[i])
	h.SetMarkerSize(0)
	h.Sumw2(False)
	i+=1
	print h
	
c1.Print("Test.root")
c1.Print("Test.pdf")


c1 = f.Get("c_Fit_K_1.0")

Colours = [1,2,3,4,6]

i = 0
for h in c1.GetListOfPrimitives():
	
	if not h.InheritsFrom("TH1"):
		continue
	
	if not h.GetName().startswith("Prediction"):
		h.SetLineWidth(0)
		h.SetMarkerSize(0)
		continue
	
	h.SetLineWidth(3)
	h.SetLineColor(Colours[i])
	h.SetMarkerSize(0)
	h.Sumw2(False)
	i+=1
	print h
	
c1.Print("Test_2.root")
c1.Print("Test_2.pdf")
