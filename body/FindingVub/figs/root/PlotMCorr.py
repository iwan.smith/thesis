from ROOT import gROOT
gROOT.SetBatch(True)
import ROOT, os, numpy as np, sys
gROOT.ProcessLine(".x lhcbStyle.C")



def MakeNice( h0, h1):
	h1.Scale(10)
	h0.Sumw2()
	
	h0.Rebin(2)
	h1.Rebin(2)
	
	h0.SetLineColor(2)
	h1.SetLineColor(1)

	h0.SetLineWidth(4)
	h1.SetLineWidth(4)

	
	h0.SetMarkerColor(2)
	h1.SetMarkerColor(1)
	
	BinWidth = h0.GetBinLowEdge(2) - h0.GetBinLowEdge(1)
	
	h0.GetYaxis().SetTitle("Candidates / ( "+"{}".format(BinWidth)[0:2]+" MeV/c^{2} )") 
	h1.GetYaxis().SetTitle("Candidates / ( "+"{}".format(BinWidth)[0:2]+" MeV/c^{2} )")

	h0.GetXaxis().SetTitle("Corrected K#mu Mass [MeV/c^{2}]") 
	h1.GetXaxis().SetTitle("Corrected K#mu Mass [MeV/c^{2}]") 
	
	h0.GetYaxis().SetTitleOffset(h0.GetYaxis().GetTitleOffset()*1.5)
	h1.GetYaxis().SetTitleOffset(h1.GetYaxis().GetTitleOffset()*1.5)


c = ROOT.TCanvas("c1", "", 1600, 1600)

c.SetTopMargin  (c.GetTopMargin()  *1.2)
c.SetLeftMargin (c.GetLeftMargin() *1.5)
c.SetRightMargin(c.GetRightMargin()*1.2)

c.Print("../MCORR_Cuts.pdf[")

###################################################

f = ROOT.TFile.Open("MCORR__Strip.root")
h0 = f.Get("h1").Clone()
h0.SetDirectory(0)

f = ROOT.TFile.Open("MCORR_SS_Strip.root")
h1 = f.Get("h1").Clone()
h1.SetDirectory(0)

MakeNice(h0, h1)

h1.Draw()
h0.Draw("SAME")


leg = ROOT.TLegend(0.7, 0.7, 0.95, 0.9, "", "NDC")
leg.SetFillColorAlpha(0,0)
leg.AddEntry(h1, "K^{-}#mu^{-}", "lep")
leg.AddEntry(h0, "K^{-}#mu^{+}", "lep")
leg.Draw()



c.Print("../MCORR_Cuts.pdf")


###################################################

f = ROOT.TFile.Open("MCORR__Strip_Iso.root")
h0 = f.Get("h1").Clone()
h0.SetDirectory(0)

f = ROOT.TFile.Open("MCORR_SS_Strip_Iso.root")
h1 = f.Get("h1").Clone()
h1.SetDirectory(0)

MakeNice(h0, h1)

h1.Draw()
h0.Draw("SAME")

leg = ROOT.TLegend(0.7, 0.7, 0.95, 0.9, "", "NDC")
leg.SetFillColorAlpha(0,0)
leg.AddEntry(h1, "K^{-}#mu^{-}", "lep")
leg.AddEntry(h0, "K^{-}#mu^{+}", "lep")
leg.Draw()


c.Print("../MCORR_Cuts.pdf")


###################################################

f = ROOT.TFile.Open("MCORR__Strip_Iso_BDT.root")
h0 = f.Get("h1").Clone()
h0.SetDirectory(0)

f = ROOT.TFile.Open("MCORR_SS_Strip_Iso_BDT.root")
h1 = f.Get("h1").Clone()
h1.SetDirectory(0)

MakeNice(h0, h1)

h1.Draw()
h0.Draw("SAME")

leg = ROOT.TLegend(0.7, 0.7, 0.95, 0.9, "", "NDC")
leg.SetFillColorAlpha(0,0)
leg.AddEntry(h1, "K^{-}#mu^{-}", "lep")
leg.AddEntry(h0, "K^{-}#mu^{+}", "lep")
leg.Draw()


c.Print("../MCORR_Cuts.pdf")

###################################################

f = ROOT.TFile.Open("MCORR__Strip_Iso_BDT_MCORRErr.root")
h0 = f.Get("h1").Clone()
h0.SetDirectory(0)

f = ROOT.TFile.Open("MCORR_SS_Strip_Iso_BDT_MCORRErr.root")
h1 = f.Get("h1").Clone()
h1.SetDirectory(0)

MakeNice(h0, h1)

h1.Draw()
h0.Draw("SAME")

leg = ROOT.TLegend(0.7, 0.7, 0.95, 0.9, "", "NDC")
leg.SetFillColorAlpha(0,0)
leg.AddEntry(h1, "K^{-}#mu^{-}", "lep")
leg.AddEntry(h0, "K^{-}#mu^{+}", "lep")
leg.Draw()


c.Print("../MCORR_Cuts.pdf")





c.Print("../MCORR_Cuts.pdf]")


