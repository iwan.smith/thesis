
#include "TH1F.h"
#include "TFile.h"
#include "TChain.h"

#include <string>

void PlotMCORR()
{
	
	for(int i = 0; i <2; i++ )
	{
		
		string Mode = i? "SS" : "";
		// Draw Stripping MCORR


		TChain* c = new TChain(("Bs2KmuNu" + Mode + "Tuple/DecayTree").c_str());

		c->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_RAW_Nov17/*_data12_Bs2KMuNu_*.root");
		TH1F* h = new TH1F("h1", "", 100, 2500, 7000);
		c->Draw("Bs_MCORR>>h1", "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS");

		TFile* f = TFile::Open(("MCORR_" + Mode + "_Strip.root").c_str(), "RECREATE");
		h->Write();
		f->Close();

		// Draw Iso/Veto MCORR

		c = new TChain(("Bs2KmuNu" + Mode + "Tuple").c_str());
		c->Add("root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_DATATUPLES_TRIMMED_Nov17/Merged_data12_Bs2KMuNu_*.root");

		h = new TH1F("h1", "", 100, 2500, 7000);
		c->Draw("Bs_MCORR>>h1", "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS && (kaon_m_PX*muon_p_PX>0 || kaon_m_PY*muon_p_PY>0) && min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9");
		
		f = TFile::Open(("MCORR_" + Mode + "_Strip_Iso.root").c_str(), "RECREATE");
		h->Write();
		f->Close();

		// Draw Iso/Veto/BDT MCORR

		h = new TH1F("h1", "", 100, 2500, 7000);
		c->Draw("Bs_MCORR>>h1", "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS && (kaon_m_PX*muon_p_PX>0 || kaon_m_PY*muon_p_PY>0) && min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_Charge_BDT_New > 0.1 && TMVA_SS_Afc_BDT_New > 0.05");
		f = TFile::Open(("MCORR_" + Mode + "_Strip_Iso_BDT.root").c_str(), "RECREATE");
		h->Write();
		f->Close();

		// Draw Iso/Veto/BDT/MCORRERR MCORR

		h = new TH1F("h1", "", 100, 2500, 7000);
		c->Draw("Bs_MCORR>>h1", "Bs_Hlt2TopoMu2BodyBBDTDecision_TOS && (kaon_m_PX*muon_p_PX>0 || kaon_m_PY*muon_p_PY>0) && min(kaon_m_IsoMinBDT,muon_p_IsoMinBDT)> -0.9 && TMVA_Charge_BDT_New > 0.1 && TMVA_SS_Afc_BDT_New > 0.05 && Bs_MCORRERR < 150");
		f = TFile::Open(("MCORR_" + Mode + "_Strip_Iso_BDT_MCORRErr.root").c_str(), "RECREATE");
		h->Write();
		f->Close();
	}
}
