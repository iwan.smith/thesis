import ROOT


ROOT.gROOT.ProcessLine(".x lhcbStyle.C")

f = ROOT.TFile.Open("/afs/cern.ch/user/i/ismith/ANA/BsToKMuNu/utils/Histogrammer/Histograms_Efficiency.root")

h_NoRW  = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("Nominal"))

h       = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("RW_Kin"))
h_D     = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("Default"))

h_Rus   = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("RW_Kin_Rusov"))
h_D_Rus = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("Default_Rusov"))

h_Bou   = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("RW_Kin_Bouchard"))
h_D_Bou = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("Default_Bouchard"))

h_Wit   = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("RW_Kin_Witzel"))
h_D_Wit = f.Get("{}/KMuNu/MC_13512010/No_Q2_6_BDT2/h_Bs_MCORR".format("Default_Witzel"))

print h_NoRW  
print h     
print h_D
print h_Rus
print h_D_Rus
print h_Bou
print h_D_Bou
print h_Wit
print h_D_Wit

h_NoRW2 = h_NoRW.Clone()
h_NoRW3 = h_NoRW.Clone()

h_NoRW  .Sumw2( False )
h       .Sumw2( False )      
#h_D     .Sumw2( False )
h_Rus   .Sumw2( False )
#h_D_Rus .Sumw2( False ) 
h_Bou   .Sumw2( False ) 
#h_D_Bou .Sumw2( False ) 
h_Wit   .Sumw2( False ) 
#h_D_Wit .Sumw2( False )


h_NoRW  .Scale( 1.0/h_NoRW  .Integral() )
h       .Scale( 1.0/h       .Integral() )
h_D     .Scale( 1.0/h_D     .Integral() )
h_Rus   .Scale( 1.0/h_Rus   .Integral() )
h_D_Rus .Scale( 1.0/h_D_Rus .Integral() )
h_Bou   .Scale( 1.0/h_Bou   .Integral() )
h_D_Bou .Scale( 1.0/h_D_Bou .Integral() )
h_Wit   .Scale( 1.0/h_Wit   .Integral() )
h_D_Wit .Scale( 1.0/h_D_Wit .Integral() )



h_NoRW  .SetFillColorAlpha(1,0.2)
h_NoRW  .SetLineWidth(0)
h       .SetLineWidth(1)
h_D     .SetLineWidth(1)
h_Rus   .SetLineWidth(1)
h_D_Rus .SetLineWidth(1)
h_Bou   .SetLineWidth(1)
h_D_Bou .SetLineWidth(1)
h_Wit   .SetLineWidth(1)
h_D_Wit .SetLineWidth(1)

h_NoRW  .SetLineColorAlpha(1, 1.0)
h       .SetLineColorAlpha(2, 1.0)
h_D     .SetLineColorAlpha(2, 1.0)
h_Rus   .SetLineColorAlpha(3, 1.0)
h_D_Rus .SetLineColorAlpha(3, 1.0)
h_Bou   .SetLineColorAlpha(4, 1.0)
h_D_Bou .SetLineColorAlpha(4, 1.0)
h_Wit   .SetLineColorAlpha(6, 1.0)
h_D_Wit .SetLineColorAlpha(6, 1.0)

h_NoRW  .SetMarkerSize(0)
h       .SetMarkerSize(0)
h_D     .SetMarkerSize(0)
h_Rus   .SetMarkerSize(0)
h_D_Rus .SetMarkerSize(0)
h_Bou   .SetMarkerSize(0)
h_D_Bou .SetMarkerSize(0)
h_Wit   .SetMarkerSize(0)
h_D_Wit .SetMarkerSize(0)


h_NoRW  .Rebin(2)
h       .Rebin(2)
h_D     .Rebin(2)
h_Rus   .Rebin(2)
h_D_Rus .Rebin(2)
h_Bou   .Rebin(2)
h_D_Bou .Rebin(2)
h_Wit   .Rebin(2)
h_D_Wit .Rebin(2)


h_NoRW  .GetXaxis().SetRangeUser(2500, 5800)
h       .GetXaxis().SetRangeUser(2500, 5800)
h_D     .GetXaxis().SetRangeUser(2500, 5800)
h_Rus   .GetXaxis().SetRangeUser(2500, 5800)
h_D_Rus .GetXaxis().SetRangeUser(2500, 5800)
h_Bou   .GetXaxis().SetRangeUser(2500, 5800)
h_D_Bou .GetXaxis().SetRangeUser(2500, 5800)
h_Wit   .GetXaxis().SetRangeUser(2500, 5800)
h_D_Wit .GetXaxis().SetRangeUser(2500, 5800)


c = ROOT.TCanvas("c", "c", 1600, 1200)

h_NoRW.SetMinimum(0)
h_NoRW.SetMaximum(h_NoRW.GetMaximum()*1.2)
h_NoRW.GetXaxis().SetTitle("Corrected K^{-} #mu^{+} Mass [MeV/c^{2}]")
h_NoRW  .Draw("HIST")
h       .Draw("HIST SAME")
h_D     .Draw("E SAME")
h_Rus   .Draw("HIST SAME")
h_D_Rus .Draw("E SAME")
h_Bou   .Draw("HIST SAME")
h_D_Bou .Draw("E SAME")
h_Wit   .Draw("HIST SAME")
h_D_Wit .Draw("E SAME")

leg1 = ROOT.TLegend(0.15, 0.65, 0.55, 0.9)
leg1.AddEntry(h,      "ISGW2", "l")
leg1.AddEntry(h_Rus,  "K&R ", "l")
leg1.AddEntry(h_Bou,  "Bouchard ", "l")
leg1.AddEntry(h_Wit,  "Flynn ", "l")

leg1.Draw()

leg2 = ROOT.TLegend(0.15, 0.3, 0.7, 0.55)
leg2.AddEntry(h_NoRW, "Uncorrected Monte Carlo", "fa")
leg2.AddEntry(h_NoRW2, "q^{2} reweighting", "l")
leg2.AddEntry(h_NoRW3, "q^{2} and kinematic reweighting", "le")
leg2.SetFillColorAlpha(0,0)
leg2.Draw()



c.Print("Bs_MCORR_Bias.pdf")