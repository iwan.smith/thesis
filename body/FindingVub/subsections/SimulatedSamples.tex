\section{Calibration Samples}
\label{sec:CalibSample}

In order to verify the accuracy of simulated Monte Carlo samples and evaluate systematics and corrections due to mismodelling in the simulation, calibration samples analogous to the signal decay are used. When considering the decay \BsToKMuNu the decay ${\Bu\to\jpsi\Kp}$ is used as a calibration sample. The \Bu decay chain may be reconstructed either by explicitly searching for a \Kp\mup\mun final state compatible with a ${\Bu\to\jpsi\Kp}$ decay or through the reconstruction of a \Km\mup pair analogous to the decay \BsToKMuNu, with the additional muon found through the use of the isolation BDT detailed in Section~\ref{ssec:ChargedTrackBDT}. The former method of reconstruction is useful for validating the efficiencies of selections dependant on the underlying event as the \BsToKMuNu and ${\Bu\to\jpsi\Kp}$ are exclusively reconstructed and the underlying event will contain no tracks compatible with the signal decays. The latter method is useful when validating the kinematics of the decay because a \BsToKMuNu decay with a non reconstructed neutrino is very difficult to distinguish from a ${\Bu\to\jpsi\Kp}$ decay with a non reconstructed muon. The decay ${\Bu\to\jpsi\Kp}$ has a high yield and it is possible to generate a highly pure data sample with very few backgrounds making it ideal for comparisons with pure Monte Carlo. The decay {$\Bu\to\jpsi\Kp$} is used to validate the Monte Carlo simulations by comparing the distributions of kinematic variables and the calculation of selection efficiencies.



\section{Combinatoric Modelling}
\label{sec:Combinatorics}

Due to the nature of partially reconstructed \B hadron decays there is no calibration sample from which a representative sample of combinatoric combinations of kaons and muons may be obtained, although there are regions of phase space where a pure sample of combinatorics may be obtained. By requiring that $m_{K\mu} > m_\Bs$, a pure combinatoric sample may be produced however the corrected mass cannot be extrapolated to lower values below the mass of the \Bs making the sample irrelevant when considering the full phase space.
 For this analysis combinatorics are modelled using a procedure termed \textit{event mixing}. A candidate kaon track from one event is combined with a candidate muon from another event and a new \Bs candidate is reconstructed from this combination. The effectiveness of this method is validated by comparing the kinematics of the mixed events with candidates in data in the region $m_{K\mu} > m_\Bs$.


%To construct a new combinatoric candidate from existing candidates each candidate is split into quarters containing the saved information on the \Bs, \Km, \mup and information on the underling event. 
%A new combinatoric candidate is created by \textit{mixing} different events together to form a new event from which a combinatoric \Bs candidate is reconstructed by combining the muon and kaon and recalculating the kinematics of the \Bs candidate.

A new combinatoric candidate is constructed by \textit{mixing} a kaon and muon candidate from different events forming a \Bs candidate simulating a combinatoric combination of the kaon and muon. When reconstructing the \textit{mixed} \Bs the momenta of the kaon and muon and primary vertex location are taken from different events. The secondary vertex location is chosen by randomly sampling the flight distance of reconstructed \Bs candidates with $m_{K\mu} > m_\Bs$ and placing it at that distance downstream of the primary vertex. The corrected mass is determined from these quantities. The uncertainty on the corrected mass is determined by randomly displacing the secondary vertex 500 times, for each displacement the corrected mass is calculated and the uncertainty is the standard deviation on the 500 corrected mass values.

%The flight distance of the new \Bs is chosen by randomly sampling the flight distance of reconstructed \Bs candidates with $m_{\Km\mup} > m_\Bs$ and the uncertainty on the corrected \Km\mup mass is calculated using a toy study; the secondary vertex location is randomly varied within its quoted uncertainty with a new value of the corrected mass calculated each time. The uncertainty on the corrected mass is the rms of the corrected mass toys.

%When reconstructing combinatoric candidates a cyclic approach is used with each event providing information that will be used in the generation of five candidates. The cyclic approach is visualised in Figure~\ref{fig:CombinatoricCombinations}. After generation of the candidates they are passed through a loose selection mimicking the preselection detailed in Section~\ref{subsec:CutPresel}.



\begin{comment}
\begin{figure}[htbp]
	\centering
	\scriptsize	
	\hspace*{-0.2cm}
	\begin{tabular}{l|l|l|l|l|l|l|l|l|l|l|l|l|l}

		Event \#   &  n-1 & n      &  n+1   & n+2   & n+3   & n+4   & n+5   & n+6   & n+8   & n+9   & n+10  & n+12  & n+15  \\\hline
		Combi. 1 &      & \cellcolor[HTML]{FF9999}\Bs &\cellcolor[HTML]{34FF34}\mup &\cellcolor[HTML]{38FFF8}\Km  &\cellcolor[HTML]{FFCB2F}Ev.  &     &     &     &     &     &     &     &     \\
		Combi. 2 &      & \cellcolor[HTML]{FE9999}\Bs &     &\cellcolor[HTML]{34FF34}\mup &     &\cellcolor[HTML]{38FFF8}\Km  &     &\cellcolor[HTML]{FFCB2F}Ev.  &     &     &     &     &     \\
		Combi. 3 &      & \cellcolor[HTML]{FE9999}\Bs &     &     &\cellcolor[HTML]{34FF34}\mup &     &     &\cellcolor[HTML]{38FFF8}\Km  &     &\cellcolor[HTML]{FFCB2F}Ev.  &     &     &     \\
		Combi. 4 &      & \cellcolor[HTML]{FE9999}\Bs &     &     &     &\cellcolor[HTML]{34FF34}\mup &     &     &\cellcolor[HTML]{38FFF8}\Km  &     &     &\cellcolor[HTML]{FFCB2F}Ev.  &     \\
		Combi. 5 &      & \cellcolor[HTML]{FE9999}\Bs &     &     &     &     &\cellcolor[HTML]{34FF34}\mup &     &     &     &\cellcolor[HTML]{38FFF8}\Km  &     &\cellcolor[HTML]{FFCB2F}Ev.  \\

	\end{tabular}
	\caption{Each event is used to generate five combinatoric combinations. For the first combination consecutive events are mixed, for the second combination alternate events are used.  }
	\label{fig:CombinatoricCombinations}
\end{figure}
\end{comment}

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.49\textwidth, page=1]{CombinatoricValidation.pdf}
	\includegraphics[width=0.49\textwidth, page=5]{CombinatoricValidation.pdf}
	\caption[Validation plots for the modelling of combinatoric candidates.]{The distributions of \Km\mup candidates in data (black) are plotted alongside simulated combinatoric candidates (red). All distributions are drawn requiring that the reconstructed \Km\mup mass is greater than $m_\Bs$.  }
	\label{fig:CombiValidation}
\end{figure}

This method of modelling combinatorics does not accurately reproduce the kinematics of the \Bs meson in the region $m_{K\mu} > m_{\Bs}$. A two dimensional reweighting is used to correct the momentum and transverse momentum of the \Bs candidate. The distributions of the \Km\mup invariant mass and corrected mass do not change as a result of the reweighting indicating the shapes of the distributions are robust. The invariant mass and corrected mass distributions with $m_{K\mu} > 5400 \mevcc$ are plotted in Figure~\ref{fig:CombiValidation} for mixed events and data.

%are invariant under a correction of the kinematics of the \Bs. 
The details of the reweighting procedure and validation plots may be found in Appendix~\ref{App:CombiValidation}.


Combinatoric candidates originating from the decay of a \bquark\bquarkbar pair will produce tracks with a large opening angle, when looking down the beam line or z axis,  the \bquark and \bquarkbar will be produced \textit{back to back} and will have opposite momenta in the transverse plane. The large opening angle obtained when selecting a kaon and muon candidate from different quarks in the \bquark\bquarkbar pair produces \Bs candidates with a large invariant mass. This feature may also be used to effectively reject combinatoric backgrounds and is discussed further in Section~\ref{ssec:Sel_Veto}.

The corrected mass of the \Km\mup pair obtained from the event mixing procedure is plotted in Figure~\ref{fig:Combi_Dist} alongside data obtained from two different triggers. Therein lies a sensitive topic with respect to the trigger. As will be discussed later in Section~\ref{ssec:CutPresel}, the trigger used for this analysis is a topological trigger which uses a multivariate selection trained to select partially reconstructed \B hadron candidates with two visible final state particles. The topological trigger uses the corrected mass of the candidate decay in its multivariate selection resulting in a reduction of reconstructed candidates with $m_\mathrm{corr} > 5800 \mevcc$. Without access to the trigger software it is impossible to fully reproduce the behaviour of the topological trigger above $m_\mathrm{corr} = 5800 \mevcc$. An additional trigger is available which selects candidates based solely on the muon and is unbiased in $m_\mathrm{corr}$. This trigger has significantly lower statistics. The corrected mass distribution of the mixed events and two trigger are plotted in Figure~\ref{fig:Combi_Dist}. Above the region in $m_\mathrm{corr}$ where no decays from \B hadrons are present the corrected mass of the mixed samples agrees incredibly well with the data originating from the muon trigger. In addition the $m_\mathrm{corr}$ distributions for both triggers are very similar below $5800\mevcc$ indicating that the topological trigger does not significantly impact the shape of the corrected mass distribution. By combining the above two arguments it is decided that the use of mixing to model combinatoric combinations of a \Km\mup pair selected using the topological trigger is valid up to {$m_\mathrm{corr} =5800 \mevcc$}. Therefore all corrected mass distributions used in this analysis will end at {$m_\mathrm{corr} =5800 \mevcc$}.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.8\textwidth, page=1]{MixingValidation.pdf}
	%\includegraphics[width=0.49\textwidth, page=6]{CombinatoricValidation_FullRange.pdf}
	%\includegraphics[width=0.49\textwidth, page=8]{CombinatoricValidation_FullRange.pdf}
	\caption{The corrected mass of a \Km\mup pair is plotted for candidates modelling combinatoric backgrounds.}
	\label{fig:Combi_Dist}
\end{figure}

\section{Simulated Samples}
\label{sec:Simulations}


A huge volume of simulated Monte Carlo decays were produced for this analysis in order to model the signal, normalisation and background decays. These are detailed in Table~\ref{Tab:MCSummary}. Signal \BsToKMuNu Monte Carlo samples are generated exclusively using the Isgur-Scora-Grinstein-Wise updated model~\cite{PhysRevD.52.6466,Kenway:1993yp} to model the form factors. \BsToKMuNu background samples include excited \Kp resonances decaying with additional neutral tracks. Monte Carlo samples are used to model the massive contributions from B hadrons decaying to charm hadrons which ultimately decay producing a kaon. The \BsToDsMuNu simulated samples contain a cocktail of $\Bs\to\Dsm\mup\neum X$ decays with each event given a flag corresponding to the correct decay type. The cocktail contains the exclusive \BsToDsMuNu decay in addition to decays including excited \Ds resonances and tauonic decay modes. Two cocktail \BsToDsMuNu samples were produced, one which had been accidentally produced with an incorrect modelling of the form factors. 

Both samples are used in this analysis. For partially reconstructed decays with more than one missing particle the corrected mass is, to first order, independent of \qsq. The form factors are defined in terms of \qsq, resulting in a corrected mass distribution which is independent of the form factor parametrisation. Both samples are used when drawing histograms of the corrected mass distribution for the background sample. When calculating efficiencies and generating template shapes of the signal mode, only the model with the correct form factor parametrisation is used. 



\begin{table}

	\small
	\centering
	\begin{tabular}{p{6cm}|p{2.5cm}|l|l}

 Decay                                   &  Year & Size  &                              \\\hhline{====}

% Signal Reconstruction
\multicolumn{4}{l}{Signal \BsToKMuNu decays plus excited \Km resonances.}                      \\\hline
$\Bs\to\Km\mup\neum $                    & 2012/2011  & 8M~-~3\invfb &                              \\
$\Bs\to\Kstarm\mup\neum $                & 2012       & 4M~-~1\invfb &                              \\
$\Bs\to K^{*-}_2\mup\neum $              & 2012/2011  & 6M~-~1\invfb &                              \\
$\Bs\to K^{*-}_0\mup\neum $              & 2012/2011  & 6M~-~1\invfb &                              \\\hline

\multicolumn{4}{l}{Signal \BsToDsMuNu decays. Samples contain a cocktail of \Bs\to\Ds decays.} \\\hline
$\Bs\to\Ds\mup\neum X$                   & 2012/2011  & 9M~-~0.2\invfb  &                              \\
$\Bs\to\Ds\mup\neum X$                   & 2012       & 20M ~-~0.4\invfb     &  Incorrect FF                \\\hline

\multicolumn{4}{l}{Charged backgrounds to \BsToKMuNu used as calibration samples.}             \\\hline

% Calibration and Background
$\Bu\to\jpsi\Kp$                         & 2012       & 20M~-~2\invfb     &                              \\
$\Bs\to\jpsi\phiz$                       & 2012       & 100M~-~40\invfb   &                              \\\hline


% Inclusive backgrounds
\multicolumn{4}{l}{Inclusive \bquark\to\cquark(\squark) decays modelling \Km\mup backgrounds.}  \\\hline
%10010015 is not used
%$\bquark\to\Kp\mup X$                    & 2012/2011  & 6M      &                             \\
$\bquark\to\cquark\to\Kp\mu X$           & 2012/2011  & 6M ~-~0.01\invfb      &                              \\
$\bquark\to\cquark\to K\mu X$            & 2012/2011  & 250k~-~0.01\invfb    &  Filtered MC                 \\
$\bquark\to\Kp\mup X$                    & 2012/2011  & 640k~-~0.03\invfb    &  Filtered MC                 \\
$\bquark\to\cquark\to\Kp\mu X$           & 2012/2011  & 1.5M~-~0.05\invfb    &  Filtered MC                 \\\hline

\multicolumn{4}{l}{Background decays reconstructible as \BsToKMuNu with misidentification.}    \\\hline
$\Lb\to\proton\mu\nu $                   & 2012       & 15M     &                              \\
$\Bd\to\pi\mu\nu$                        & 2012       & 4M      &                              \\
$\Bu\to\rho\mu\nu$                       & 2012       & 5M      &                              \\
$\Bd\to\rhom\mup\nu$                     & 2012       & 4M      &                              \\\hline

\multicolumn{4}{l}{\BsToKMuNu backgrounds containing additional charged tracks.}               \\\hline
$\Bd\to\jpsi\Kstar$                      & 2012       & 10M     &                              \\
$\Bu\to\jpsi\Kstar$                      & 2012       & 24M     &                              \\
$\Bd\to\Dstar\pi, \Dstar\to\Dz\pi$       & 2012       & 140k    &                              \\
$\Bd\to\D\pi, \D\to K \mu\nu $           & 2012       & 160k    &                              \\
$\Bd\to\Dstar\mu\nu X, \D\to K \pi\pi\pi$& 2012       & 5M      &                              \\
$\Bd\to\Dstar\mu\nu X, \D\to K \pi$      & 2012       & 7M      &                              \\
$\Bd\to\D\mu\nu X, \D\to K\pi\pi$        & 2012       & 20M     &                              \\
$\Bu\to\Dz\pi$                           & 2012       & 140k    &                              \\
$\Bu\to\Dstarz\pi, \Dstarz\to\Dz\pi $    & 2012       & 140k    &                              \\
$\Bu\to\Dstarz\pi, \Dstarz\to\Dz\g $     & 2012       & 140k    &                              \\
$\Bu\to\Dz\rho$                          & 2012       & 140k    &                              \\
$\Bu\to\Dz\mu\nu $                       & 2012       & 15M     &                              \\
$\Bs\to\Dz\Kstarz$                       & 2012       & 150k    &                              \\
$\Bc\to\Dz\mu\nu$                        & 2012       & 1M      &                              \\\hline

\multicolumn{4}{l}{\BsToDsMuNu backgrounds containing additional charged tracks.}               \\\hline
$\Bu\to\Dss\Dstar $                      & 2012       & 5M      &                              \\
$\Bu\to\D\D$                             & 2011       & 3M      &                              \\
$\Bs\to\Dz\Ds K$                         & 2012       & 5M      &                              \\
$\Bs\to\Dss\Dss$                         & 2012       & 5M      &                              \\
$\Bs\to\D\D$                             & 2011       & 1M      &                              \\
$\Bd\to\Ds\Dstar$                        & 2012       & 5M      &                              \\
$\Bd\to\D\D$                             & 2011       & 2M      &                              \\
$\Bc\to\jpsi\Dsp$                        & 2012       & 3M      &                              \\




	\end{tabular}
	\caption[A summary of the simulated samples used in this Analysis.] {A summary of the simulated samples used in this Analysis. The sample size for filtered events is counted after stripping selections are applied. An approximate luminosity is included for the most significant samples. }
	\label{Tab:MCSummary}
\end{table}






