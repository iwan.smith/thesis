\section{Kinematics}
\label{sec:Kinematics}


Semileptonic decays present a unique challenge at \lhcb. The invisible neutrino requires that all events are partially reconstructed making it impossible to reconstruct the invariant mass of the parent decay particle. Fortunately the \lhcb experiment has excellent vertex resolution allowing the \Bs production and decay vertices to be measured. With the knowledge of the \Bs flight direction one can use the geometry of the event to measure the transverse momentum of the invisible neutrino and calculate a lower limit on the mass of the \Bs meson. This is called the corrected mass, and the details of its calculation are given in Section~\ref{ssec:MCORR}. Alternatively one can use the knowledge of the true mass of the \Bs meson to reconstruct the full kinematics of the invisible neutrino with a two fold ambiguity. The details of neutrino reconstruction are given in Section~\ref{ssec:neutrinoReco}.


\subsection{Corrected Mass}
\label{ssec:MCORR}


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.5\textwidth]{McorrTopo-crop.pdf}
	\caption[Visualisation of \BsToKMuNu topology displaying both neutrino solutions.]{Visualisation of conservation of momentum with respect to the \Bs flight direction. Two neutrino solutions are compatible with the reconstructed decay. In the \Bs rest frame the two solutions are back to back in the $z$ direction, but after boosting they both travel in the positive $z$ direction.}
	\label{fig:MCORRTopo}
\end{figure}


The corrected mass is a lower limit on the mass of the \Bs momentum. As visualised in Figure~\ref{fig:MCORRTopo}, the event is rotated such that the \Bs meson flies in the $z$ direction, and from the symmetric geometry of the event the transverse momentum of the neutrino must be equal and opposite to the  transverse momentum of the visible system, 
\begin{equation}
	\begin{aligned}
	\vec{p_\perp}(\Km\mup) &= -  \vec{p_\perp}(\neum) \\
	p_\perp &\equiv |\vec{p_\perp}(\Km\mup)|
	\end{aligned}
\end{equation}
 The corrected mass is defined as
\begin{equation}
	M_{corr} = \sqrt{M^2_{X\mu} + p_\perp^2} + p_\perp,
	\label{eq:MCORR}
\end{equation}
 with uncertainty
 \begin{equation}
 	\sigma_{M_{corr}} = \left(\frac{p_\perp}{\sqrt{M^2_{X\mu} + p_\perp^2}}+1\right)\sigma_{p_\perp}
	\label{eq:MCORRErr} 	
 \end{equation}

where $M_{X\mu}$ is the invariant mass of the visible final state particles, and $p_\perp$\footnote{$p_\perp$ is the momentum transverse to the \Bs flight directions and \pt is the momentum transverse to the $z$ axis.} is the visible momentum transverse to the \Bs flight direction. If the only missing particle is a neutrino the corrected mass distribution will peak at the \Bs mass with a wide tail to the left and an immediate cut off above the mass of the \Bs. The effects of resolution on the measurement of the \Bs flight direction result in tails forming above the mass of the \Bs, the corrected mass distributions for \BsToKMuNu and several backgrounds are plotted in Figure~\ref{fig:Find_MCORR_Resolution} before and after the simulation of resolution effects.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.45\textwidth]{MCorr_Resolution_1.pdf}
	\includegraphics[width=0.45\textwidth]{MCorr_Resolution_2.pdf}
	\caption[Corrected mass distributions with and wothout resolution.]{The corrected mass distribution for simulated signal and background events reconstructed as \BsToKMuNu before (left) and after (right) the modelling of vertex resolution.}
	\label{fig:Find_MCORR_Resolution}
\end{figure}


The dominant source of uncertainty on the corrected mass comes from an uncertainty in the \B flight direction which results in a large uncertainty $p_\perp$. %The \B flight direction is reconstructed from the primary and secondary vertices, consequently the leading source of uncertainty originates from a resolution on the primary and secondary vertices, 
The uncertainty on the \B flight direction must be propagated through to the uncertainty in $p_\perp$. The propagation of uncertainties to $\sigma_{p_\perp}$ is non trivial and the calculation has not been included in this thesis. The full derivation may be found in Reference~\cite{Sutcliffe:2232639}. The corrected mass distributions for simulated events reconstructed as \BsToKMuNu are plotted in Figure~\ref{fig:Find_MCORR_Resolution} before and after the simulation of resolution effects, the very sharp signal peak becomes significantly broader and harder to resolve with the addition of resolution effects.


The resolution on the plotted corrected mass is significantly improved if one rejects events with a large corrected mass uncertainty. The distributions of signal Monte Carlo decays and same sign data candidates are plotted in Figure~\ref{fig:MCorrErr_MCORR}. The signal events passing the selection have a significantly sharper peak while the background sample of reconstructed \Km\mup candidates is shifted to the left away from the signal peak. The additional resolution and separating power obtained by rejecting events with a high corrected mass uncertainty result in reduced systematics when performing a fit to the corrected mass.

%The distribution consisting of same sign candidates is shifted to the left when rejecting events with a large uncertainty on the corrected mass and the peak corresponding to the signal decay becomes significantly sharper. 

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth, page=1]{MCorrErrValidation.pdf}
	\caption[Corrected mass distributions of events passing and failing the corrected mass uncertainty selection.]{The corrected mass distributions of Monte Carlo signal decays (red) and same sign candidates from data (blue). Events passing the $\sigma_{m_{\mathrm{corr}}}$ selection are unshaded and the events failing the selection are shaded.}
	\label{fig:MCorrErr_MCORR}
\end{figure}{}

For this analysis candidates with a corrected mass uncertainty greater than $m_{\mathrm{corr}} = 150\mevcc$ are rejected. This selection has an efficiency of approximately $45\%$ for both signal and partially reconstructed background decays alike while backgrounds from combinatoric combinations are significantly reduced. Although this selection doesn't significantly increase signal purity the separation between signal and background decays is improved in the corrected mass distribution resulting in a fit with significantly reduced systematics. The efficiency of this selection is verified using a kaon and muon combination from the decay $\Bu\to(\jpsi\to\mup\mun)\Kp$ and is quantified later in Section~\ref{ssec:JPsiKCorr}. The distribution of the corrected mass uncertainty is plotted in Figure~\ref{fig:MCorrErr_MCORRERR} for signal Monte Carlo and the \Km\mup combination from ${\Bu\to\jpsi\Kp}$ using Monte Carlo and data. As the dominant source of uncertainty on the \Bs flight direction originates from the precision on the primary and secondary vertices a \Bs meson with a longer flight distance will have a lower corrected mass uncertainty, consequently the application of this selection will introduce a bias on the measured flight distance or calculated decay time. This selection is very effective at rejecting backgrounds from combinatoric combinations of a \Km\mup pair. Combinatorics originate from two sources, the combination of prompt tracks originating from the primary vertex or from \bquark\bquarkbar production with one \bquark decaying semileptonicaly producing a muon and the other decaying hadronically producing a kaon. %Combinations formed from the former production mechanism will have a very short decay length and large corrected mass uncertainty while those produced from the latter method with form a secondary vertex with a large uncertainty resulting in a large corrected mass uncertainty. 

When considering the decay \BsToDsMuNu no selection is made on the corrected mass uncertainty of the \Km\mup pair or the \Dsm\mup pair. The dominant background to the decay \BsToDsMuNu is $\Bs\to\Dssm\mup\neum$ and a selection on the corrected mass uncertainty does little to further separate the two decays.

\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.65\textwidth, page=2]{MCorrErrValidation.pdf}
	\caption{The corrected mass uncertainty for signal decays and ${\Bu\to\jpsi\Kp}$ decays reconstructed as \BsToKMuNu.}
	\label{fig:MCorrErr_MCORRERR}
\end{figure}




\subsection{Neutrino Reconstruction and \qsq }
\label{ssec:neutrinoReco}

The $|\Vub|$ measurement will be performed by measuring the signal yield of \BsToKMuNu candidates in two regions of phase space, separated by $\qsq=7\gev^2$, where \qsq is the squared four vector momentum recoiling off the \Bs, which is equal to the four momentum squared of the \mup\neum combination. A calculation of \qsq first requires the neutrino momentum be reconstructed. The component of the neutrino momentum transverse to the \B flight direction, $p_\perp$ is equal and opposite the transverse momentum of the \Km\mup pair. The longitudinal component, $p_\parallel$, may be determined to a two-fold ambiguity with the quadratic equation
\begin{equation}
	p_\perp = p_\perp(\Km\mup)
\end{equation}
\begin{equation}
	p_\parallel = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a},
\end{equation}
where $a$, $b$ and $c$ are determined as 

\begin{equation}
	\begin{aligned}
		a &= |2p_{\parallel}m_{K\mu}|^2,                           \\
		b &= 4p_\parallel(2 p_\perp p_\parallel-m^2_{\mathrm{miss}}),       \\
		c &= 4 p_\perp( p_\parallel^2 + m^2_\Bs ) - |m^2_{\mathrm{miss}}|^2,\\
		m^2_{\mathrm{miss}} &= m_\Bs^2 - m^2_{K\mu}.                        \\ 
	\end{aligned}
\end{equation}

The kinematics of the \Bs meson and \qsq of the event may now be calculated with a two fold ambiguity~\cite{DAMBACH2006824,Stone:2014mza}. When performing a physics analysis it is desirable to resolve this ambiguity without the introduction of a bias in \qsq. A choice must be made on which of the two solutions of \qsq will be used when performing an analysis.

The simplest approach is to randomly select one of the two solutions which while unbiased has a poor resolution in \qsq. A significantly improved method uses a linear regression model to predict the \Bs momentum and the ambiguity is resolved by selecting the solution most consistent with the regression value. The full details of the regression method are given below.


Due to the detector resolution effects approximately 20\% of the candidates have an unphysical solution (i.e. $b^2 < 4ac$) for $P_\parallel$. The unphysical events fall into corrected mass region above the \Bs invariant mass, $m_{corr}(\Bs) > m(\Bs)$, and are removed when restricting events to a specific region in \qsq. 


\subsection{Linear Regression to Reconstruct \qsq}
\label{ssec:neutrinoRegression}



Linear regression analysis is a statistical technique for predicting the value of a target or response variable based on relationships with predictor or regressor variables~\cite{rencher_christensen_2012,freedman_2009,montgomery_peck_vining_2013}. For this analysis the momentum of the \Bs is inferred from the flight distance and polar angle of the \Bs with a resolution of 60\% which is sufficient to select the correct solution of the quadratic equation 70\% of the time~\cite{Ciezarek:2016lqu}, compared to the random selection which selects the correct solution 50\% of the time.



\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.48\textwidth, page=4]{body/FindingVub/figs/Qsq.pdf}
	\includegraphics[width=0.48\textwidth, page=5]{body/FindingVub/figs/Qsq.pdf}
	\caption{Input variables used to predict the \Bs momentum with a linear regression model.}
	\label{fig:QsqRegInput}
\end{figure}

The \Bs momentum is weakly correlated to its polar angle, $\theta_\mathrm{flight}$, 
\begin{equation}
	P = \frac{p_T}{\sin\theta_\mathrm{flight}},
\end{equation}
and flight distance, $|\vec{F}|$, and decay time, $t$,
\begin{equation}
	P=\frac{M|\vec{F}|}{t},
\end{equation}
as shown in Figure~\ref{fig:QsqRegInput}. The two flight variables are considered in a least squares linear regression model~\cite{2012arXiv1201.0490P}
\begin{equation}
	P = \beta_0 + \beta_1 /\sin\theta_{\mathrm{flight}} + \beta_2|\vec{F}| + \varepsilon,
\end{equation}
where $\beta_n$ are parameters to be determined, and $\varepsilon$ is a random component with a mean of $0$ and variance equal to the variance of the predicted momentum. 
The predicted value of the \Bs momentum is compared to the two solutions derived from the quadratic equation defined in Section~\ref{ssec:neutrinoReco} and the solution closest to the regression value, $\qsq_{\mathrm{Best}}$, is selected. 
The use of regression in the selection of a solution to the quadratic equation significantly improves the resolution on the reconstructed true \qsq as plotted in Figure~\ref{fig:QsqRegInput2}. The resolution on the reconstructed \qsq for different methods of selecting a solution is given in Table~\ref{tab:qsqres}. Using the output of the linear regression model to select a solution improves the resolution on the reconstructed \qsq by 38\% when compared to a random selection.


\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.45\textwidth, page=1]{body/FindingVub/figs/Qsq.pdf}
	\includegraphics[width=0.45\textwidth, page=3]{body/FindingVub/figs/Qsq.pdf}
	\caption[True \qsq distributions from Monte Carlo reconstructed in various ways.]{True \qsq distributions from Monte Carlo (shaded green) with the reconstructed \qsq with different methods of selecting the \Bs momentum solution (left) and the resolution on \qsq is shown using different selections (right). The best solution is the solution closest to the regression value, and the worst solution is furthest from the regression value.}
	\label{fig:QsqRegInput2}
\end{figure}




\begin{table}[hbt]
	\centering
	\begin{tabular}{ll}
	Solution    & RMS \\\hline
	Correct     &  $1.07 ~\mathrm{GeV^2/c^4}$ \\
	Regression  &  $2.21 ~\mathrm{GeV^2/c^4}$ \\
	Random      &  $3.06 ~\mathrm{GeV^2/c^4}$ \\
	Incorrect   &  $4.23 ~\mathrm{GeV^2/c^4}$ \\
	\end{tabular}
	\caption{Resolution on reconstructed \qsq after selecting one of the two solutions. Resolutions are given for the correct solution, solution obtained from regression, randomly selecting a solution and the incorrect solution.}
	\label{tab:qsqres}
\end{table}




\begin{comment}
\begin{figure}[htbp]
	\centering
	\includegraphics[width=0.45\textwidth, page=1]{body/FindingVub/figs/Qsq_BinMigration.pdf}
	\includegraphics[width=0.45\textwidth, page=2]{body/FindingVub/figs/Qsq_BinMigration.pdf}
	\caption{The true \qsq distributions after splitting the data into high and low \qsq bins using the best method of reconstructing the \Bs momentum and a bin boundary of $7 \gev^2/c^4$.}
	\label{fig:QsqRegInput3}
\end{figure}
\end{comment}
