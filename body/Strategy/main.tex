\input{body/Strategy/title}


This chapter outlines the strategy used for the measurement of \AbsVub at the \lhcb experiment presented in this thesis. This includes the motivation for the choice to measure \AbsVub with the decay \BsToKMuNu.


An exclusive approach is used to measure \AbsVub at \lhcb instead of an inclusive approach for several reasons. The \lhcb environment contains huge amounts $\bquark\to\cquark$ decays which completely mask the $\bquark\to\uquark$ inclusive signal, in addition at the \lhcb experiment it is not possible to exploit the $\bquark\to\uquark$ kinematic endpoint, the approach used by the $B$ factories. The \BsToKMuNu decay was chosen over the decay {\Bs\to\pim\mup\neum} as it is easier to positively identify a kaon and there are fewer backgrounds. The decay \BsToDsMuNu was chosen as it most closely resembles the \BsToKMuNu decay, but with a {$\bquark\to\cquark$} transition. By placing a selection on the invariant mass of the final state particles decaying from the \Ds a data sample is produced containing very few additional backgrounds. 

Over 600 billion \bbbar pairs were produced at \lhcb during 2012, with $\approx 8.2\%$ of \bquark quarks fragmenting into \Bs mesons~\cite{PhysRevD.85.032008,PhysRevLett.118.052002}. The high branching fraction of $\bquark\to\uquark\ell\neu$ processes, $\approx 10^{-4}$,  creates a high statistics environment in which $\bquark\to\uquark$ transitions can be measured allowing for novel determinations of \AbsVub from the decay \BsToKMuNu. 



To measure \AbsVub solely using the decay \BsToKMuNu a precise measurement of the \bbbar cross section at the \lhc is needed in addition to a precise measurement of the integrated luminosity. While both of these measurements have been performed they are not precise enough to perform a competitive measurement of \AbsVub. Instead a normalisation is made to \BsToDsMuNu decays and a measurement of the ratio of branching fractions is performed restricting the \BsToKMuNu decays to a region in \qsq.  This measurement, when combined with form factor predictions from lattice QCD and light-cone sum rules, allows for a determination of \VubVcbsq. Lattice QCD form factor predictions are used when restricting to a high \qsq, $\qsq > \qsqcut\gevgevcccc$ and light-cone sum rules are used when restricting to low \qsq, $\qsq < \qsqcut\gevgevcccc$. An experimental measurement of the ratio of branching fractions is performed. There are three driving factors which determine the ratio of branching fractions, the ratio of CKM matrix elements $\AbsVub/\AbsVcb$, the kinematics and phase space of the decays, and the calculation of form factors for the decays. The kinematic and phase space dependencies and form factors are combined into a single term which is referred to as the ratio of form factors, $R_{FF}$, which fully encapsulates the theoretical contributions to the ratio of branching fractions.
\VubVcbsq is determined according to:

\begin{equation}
	\frac{|\Vub|^2}{|\Vcb|^2} = \frac{\BF(\BsToKMuNu)_{|\qsq>\qsqcut \gevgevcccc}}{\BF({\BsToDsMuNu})} \times R_{FF}^{LQCD}
	\label{eq:Vub_FF}
\end{equation}
and
\begin{equation}
	\frac{|\Vub|^2}{|\Vcb|^2} = \frac{\BF(\BsToKMuNu)_{|\qsq<\qsqcut \gevgevcccc}}{\BF({\BsToDsMuNu})} \times R_{FF}^{LCSR}
	\label{eq:Vub_FF_LCSR}
\end{equation}


where the branching fractions, \BF,  and form factor ratio, $R_{FF}$, are written using equations~\ref{eq:dGamma_KMuNu} and ~\ref{eq:dGamma_DsMuNu},
\begin{equation}
	R_{FF}^{LQCD} = \frac{\int_{m_\mu^2}^{(m_\Bs-m_\Ds)^2} \frac{1}{|\Vcb|^2} \frac{d\Gamma}{d\qsq}_{\BsToDsMuNu}d\qsq}{\int_{7\gevgevcccc}^{(m_\Bs-m_\Km)^2} \frac{1}{|\Vub|^2} \frac{d\Gamma}{d\qsq}_{\BsToKMuNu}d\qsq},\\
	\label{eq:Rff_LQCD}
\end{equation}
and
\begin{equation}
	R_{FF}^{LCSR} = \frac{\int_{m_\mu^2}^{(m_\Bs-m_\Ds)^2} \frac{1}{|\Vcb|^2} \frac{d\Gamma}{d\qsq}_{\BsToDsMuNu}d\qsq}{\int_{m_\mu^2}^{7\gevgevcccc} \frac{1}{|\Vub|^2} \frac{d\Gamma}{d\qsq}_{\BsToKMuNu}d\qsq}.\\
		\label{eq:Rff_LCSR}		
\end{equation}
The values of \AbsVub and \AbsVcb in equations ~\ref{eq:Rff_LQCD} and ~\ref{eq:Rff_LCSR} cancel with the terms in equations~\ref{eq:dGamma_KMuNu} and~\ref{eq:dGamma_DsMuNu}, producing a quantity which may be derived from theoretical calculations.
The calculated form factors are,
\begin{equation}
	R_{FF}^{LQCD} = 0.46 \pm 0.11,
\end{equation}
and
\begin{equation}
	R_{FF}^{LCSR} = 0.48 \pm 0.06.
\end{equation}



When considering the decay \BsToDsMuNu the integration ranges are  \\${m_\mup^2 \to  (m_\Bs - m_\Ds)^2}$.

The choice to measure the decay using two bins in \qsq is motived by the fact that the form factor predictions from LQCD are most precise at high \qsq (models differ by an order of magnitude at low \qsq) and the predictions from LCSR are most precise at low \qsq.

The ratio of branching fractions is measured experimentally by taking the ratio of the yields of signal events and combining with their relative efficiencies and branching fractions of intermediate decays,
\begin{equation}
\begin{split}
	&\frac{\BF(\BsToKMuNu)}{\BF(\BsToDsMuNu)}\\
	&=\frac{N_\BsToKMuNu}{N_\BsToDsMuNuFull} \cdot \frac{\varepsilon_\BsToDsMuNuFull}{\varepsilon_\BsToKMuNu} \cdot \BF(\DsToKKPi)
\end{split}
\end{equation}
where $N_\BsToKMuNu$ and $N_\BsToDsMuNuFull$ are the signal yields after applying all selections, $\varepsilon_\BsToKMuNu$ and $\varepsilon_\BsToDsMuNuFull$ are the selection efficiencies for the decays \BsToKMuNu and \BsToDsMuNuFull respectively. 

The signal yields are determined by performing fits to the corrected mass distributions of selected \Km\mup and \Dsm\mup candidates and the efficiencies are determined from simulation after a series of data driven corrections are applied. All efficiencies are taken with respect to the specified \qsq selection.

The \DsToKKPi branching fraction is taken from the PDG and is a weighted average of three measurements from the \cleo, \belle, and \babar experiments~\cite{PDG:2016xqp},
\begin{equation}
	\BF(\DsToKKPi) = (5.44 \pm 0.18) \times10^{-2}
\end{equation}



