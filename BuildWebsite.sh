mkdir www/chapters -p

mv chapters/introduction.pdf  www/chapters/01_introduction.pdf
mv chapters/theory.pdf        www/chapters/02_theory.pdf
mv chapters/detector.pdf      www/chapters/03_detector.pdf
mv chapters/Strategy.pdf      www/chapters/04_Strategy.pdf
mv chapters/FindingVub.pdf    www/chapters/06_FindingVub.pdf
mv chapters/VubAnalysis.pdf   www/chapters/07_VubAnalysis.pdf
mv chapters/Conclusion.pdf    www/chapters/09_Conclusion.pdf
mv chapters/*pdf www/chapters/. -v


cp thesis.pdf www/thesis.pdf

BUILDDATE=`date`
BUILDNUMBER=`cat ~/ThesisLog/Date.log |wc -l`


cat html/Header.html|sed -e "s/BUILDNUMBER/$BUILDNUMBER/g"|sed -e "s/BUILDDATE/$BUILDDATE/g" > www/index.html

cd www 
for F in `find chapters -iname '*pdf'`
do
	echo "<a href=\"$F\">"$F"</a><br>" >> index.html
done
cd ..

cat html/Footer.html >> www/index.html


bash ThesisUnitTests.sh
mv Errors_* www/.
mv BuildLog.txt www/.

rm ~/ThesisWebsite -rfv
cp -rfv www ~/ThesisWebsite
rm www -rfv

exit 0
